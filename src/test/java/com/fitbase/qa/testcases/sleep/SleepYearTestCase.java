package com.fitbase.qa.testcases.sleep;

import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.fitbasemobile.qa.actions.LoginAction;
import com.fitbasemobile.qa.actions.SleepAction;
import com.fitbasemobile.qa.helpers.BaseClass;
import com.fitbasemobile.qa.testdata.AppDataProvider;
import com.fitbasemobile.qa.utility.Log;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class SleepYearTestCase {
	static AndroidDriver<AndroidElement> driver;

@BeforeClass
public void fitbaseAppLaunch() throws Exception{
	PropertyConfigurator.configure("log4j.properties");	
	driver = BaseClass.LanuchFitbaseMobileApp();
	Log.info("Fitbase App Launched Successfully");}

@Test(priority = 0, dataProvider = "FitbaseLogin" , dataProviderClass = AppDataProvider.class)
public void verifyfitbaseAppYearWaterDashboard(String Username ,String Password) throws Exception{
	LoginAction.fitbaseMobileAppLoginTestCase(driver,Username,Password);
	SleepAction.fitbaseAppSleepYearPageTestCase(driver);
	LoginAction.fitbaseMobileAppLogoutTestCase(driver);
	Log.info("Successfully validated Fitbase Login functionality with valid Credentials");}

@AfterClass 
public void FitbaseAppCloseApp() throws Exception{
	driver.quit(); 
	Log.info("Fitbase App closed succesfully");}
}
