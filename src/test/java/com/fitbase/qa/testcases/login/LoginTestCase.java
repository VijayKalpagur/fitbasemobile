package com.fitbase.qa.testcases.login;

import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.fitbasemobile.qa.actions.LoginAction;
import com.fitbasemobile.qa.helpers.BaseClass;
import com.fitbasemobile.qa.testdata.AppDataProvider;
import com.fitbasemobile.qa.utility.Log;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class LoginTestCase {
	 
	static AndroidDriver<AndroidElement> driver;

@BeforeClass
public void fitbaseAppLaunch() throws Exception{
	PropertyConfigurator.configure("log4j.properties");	
	driver = BaseClass.LanuchFitbaseMobileApp();
	Log.info("Fitbase App Launched Successfully");}

//@Test(priority = 0) 
//public void verifyFitbaseAppHomePage() throws Exception {
//	LoginAction.fitbaseMobileAppHomePageTestCase(driver);
//	Log.info("FMM Login Home Page Sucessfully Verified");}
//
//@Test(priority = 1) 
//public void verifyFitbaseAppLoginPage() throws Exception {
//	LoginAction.fitbaseMobileLoginPageTestCase(driver);
//	Log.info("FMM Login Page Sucessfully Verified");}
//
//@Test(priority = 2) 
//public void verifyFitbaseAppLoginWithValidCredentials() throws Exception {
//	LoginAction.fitbaseMobileAppHomePageTestCase(driver);
//	Log.info("FMM Login With Valid Credentials Sucessfully Verified");}

//@Test(priority = 3, dataProvider = "FitbaseLogin" , dataProviderClass = AppDataProvider.class)
//public void verifyFitbaseAppLoginPasswordEyeIcon(String Username ,String Password) throws Exception{
//	LoginAction.fitbaseMobileAppPasswordEyeIconTestCase(driver,Username,Password);
//	Log.info("Login Password Eye Icon Verfied with Text Visibility Off and On");}
////	  	 	
@Test(priority = 0, dataProvider = "FitbaseLogin" , dataProviderClass = AppDataProvider.class)
public void verifyFitbaseAppValidLogin(String Username ,String Password) throws Exception{
	LoginAction.fitbaseMobileAppLoginTestCase(driver, Username, Password);
//	LoginAction.fitbaseMobileAppLogoutTestCase(driver);
	Log.info("Successfully validated Fitbase Login functionality with valid Credentials");}
//
//@Test(priority = 5) 
//public void verifyFitbaseAppForgotPasswordPage() throws Exception {
//	 LoginAction.fitbaseMobileAppForgotPasswordLinkTestCase(driver);
//	 LoginAction.fitbaseMobileAppForgotPasswordPageTestCase(driver);
//	 Log.info("Forgot Password Page Sucessfully Verified");} 
//	 
//@Test(priority =6,dataProvider = "ForgotPasswordValidTestData" ,dataProviderClass = AppDataProvider.class) 
//public void verifyFitbaseAppForgotPasswordValidEmailTestCase(String Email) throws Exception {
//	 LoginAction.fitbaseMobileAppForgotPasswordLinkTestCase(driver);
//	 LoginAction.forgotPasswordValidEmailTestCase(driver,Email);
//	 Log.info("Forgot Password Verified Sucessfully with Valid Email Address");}

//@AfterClass 
//public void FitbaseAppCloseApp() throws Exception{
//	driver.quit(); 
//	Log.info("Fitbase App closed succesfully");}
	 }

