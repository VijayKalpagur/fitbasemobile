package com.fitbasemobile.qa.actions;

import org.testng.Assert;

import com.fitbasemobile.qa.pages.SleepPage;
import com.fitbasemobile.qa.utility.Log;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class SleepAction {
	
public static void fitbaseAppDaySleepPageTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
		
	SleepPage Sleep = new SleepPage(driver);
		
	Sleep.FitbaseAppMenuIcon.click();
	Log.info("Click action performed on Menu icon");
			
	Thread.sleep(1500);
			
	Actions.menuPageScrollup(driver);
		
	Assert.assertTrue(Sleep.FitbaseAppSleepBtn.isDisplayed());
	Sleep.FitbaseAppSleepBtn.click();
	Log.info("Click Action Performed on Sleep Module Tab");  
	
	Thread.sleep(3000);
		    
	Assert.assertTrue(Sleep.FitbaseAppSleepHeadeText.isDisplayed());
	Log.info("Fitbase App Sleep Head Text Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppLogSleepLink.isDisplayed());
	Assert.assertTrue(Sleep.FitbaseAppLogSleepLink.isEnabled());
	Log.info("Fitbase App Log Sleep Link Verfied");
			
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTabText.isDisplayed());
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTabText.isEnabled());
	Log.info("Fitbase App Day Sleep Tab Text Verfied");

	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodayText.isDisplayed());
	Log.info("Fitbase App Day Today Text Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodayForwardBackwardicon.isDisplayed());
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodayForwardBackwardicon.isEnabled());
	Log.info("Fitbase App Day Sleep Today Forward Backward icons Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTimeRecommendedText.isDisplayed());
	Log.info("Fitbase App Day Sleep Time Recommended Text Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepCoudMoonIcon.isDisplayed());
	Log.info("Fitbase App Day Sleep Coud Moon Icon Verfied");

	Assert.assertTrue(Sleep.FitbaseAppDaySleepTotalSleptTimeText.isDisplayed());
	Log.info("Fitbase App Day Sleep Total Slept Time Text Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepAwakeTextandTime.isDisplayed());
	Log.info("Fitbase App Day Sleep Awake Text and Time Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepLightTextandTime.isDisplayed());
	Log.info("Fitbase App Day Sleep Light Text and Time Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepDeepTextandTime.isDisplayed());
	Log.info("Fitbase App Day Sleep Deep Text and Time Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppDaySleepGraph.isDisplayed());
	Log.info("Fitbase App Day Sleep Graph Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepMoonIconWithtime.isDisplayed());
	Log.info("Fitbase App Day Sleep Moon Icon With time Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepSunIconWithtime.isDisplayed());
	Log.info("Fitbase App Day Sleep Sun Icon With time Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepWakeLightDeepText.isDisplayed());
	Log.info("Fitbase App Day Sleep Wake Light Deep Text Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPercentageSleepEfficiency.isDisplayed());
	Log.info("Fitbase App Day Sleep Percentage Sleep Efficiency Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepMoodIcon.isDisplayed());
	Log.info("Fitbase App Day Sleep Mood Icon");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepMoodText.isDisplayed());
	Log.info("Fitbase App Day Sleep Mood Text");
		
	Actions.menuPageScrollup(driver);
	
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepHeaderText.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Header Text Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepCloudMoonIcon.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Cloud Moon Icon Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepTimeRange.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Time Range Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepTime.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Time");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepThruDevices.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Thru Devices Verfied");
		
	Thread.sleep(3000);	}

public static void fitbaseAppDayLogSleepPageTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
		
	SleepPage Sleep = new SleepPage(driver);
		
	Sleep.FitbaseAppMenuIcon.click();
	Log.info("Click action performed on Menu icon");
			
	Thread.sleep(1500);
			
	Actions.menuPageScrollup(driver);
		
	Assert.assertTrue(Sleep.FitbaseAppSleepBtn.isDisplayed());
	Sleep.FitbaseAppSleepBtn.click();
	Log.info("Click Action Performed on Sleep Module Tab");  
		
	Thread.sleep(1000);
		
	Assert.assertTrue(Sleep.FitbaseAppLogSleepLink.isDisplayed());
		
	Thread.sleep(1000);
	Sleep.FitbaseAppLogSleepLink.click();
	Log.info("Fitbase App Log Sleep Link Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDayLogSleepPopupWindow.isDisplayed());
	Log.info("Fitbase App Day Log Sleep Popup Window");
	
	Assert.assertTrue(Sleep.FitbaseAppDayLogSleepPageHeaderText.isDisplayed());
	Log.info("Fitbase App Day Log Sleep Page Header Text");
		
	Assert.assertTrue(Sleep.FitbaseAppDayLogSleepPageCancelIcon.isDisplayed());
	Log.info("Fitbase App Day Log Sleep Page Cancel Icon Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDayLogSleepPageDate.isDisplayed());
	Log.info("Fitbase App Day Log Sleep Page Date");
		
	Assert.assertTrue(Sleep.FitbaseAppDayLogSleepPageBedTime.isDisplayed());
	Log.info("Fitbase App Day Log Sleep Page BedTime");

	Assert.assertTrue(Sleep.FitbaseAppDayLogSleepPageBedTimeIcon.isDisplayed());
	Log.info("Fitbase App Day Log Sleep Page Bed Time Icon");
		
	Assert.assertTrue(Sleep.FitbaseAppDayLogSleepPageWakeUpTime.isDisplayed());
	Log.info("Fitbase App Day Log Sleep Page WakeUp Time");
		
	Assert.assertTrue(Sleep.FitbaseAppDayLogSleepPageWakeUpTimeIcon.isDisplayed());
	Log.info("Fitbase App Day Log Sleep Page WakeUp Time Icon Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDayLogSleepPageSleepDuration.isDisplayed());
	Log.info("Fitbase App Day Log Sleep Page Sleep Duration");
		
	Assert.assertTrue(Sleep.FitbaseAppDayLogSleepPageSleepDurationIcon.isDisplayed());
	Log.info("Fitbase App Day Log Sleep Page Sleep Duration Icon");
		
	Sleep.FitbaseAppDayLogSleepPageSaveButton.click();
	Log.info("Click Action Performed on Save Button");  
		
//	Sleep.FitbaseAppAddDrinkPageCancelIcon.click();
//	Log.info("Click action performed on Cancel Icon");
	Thread.sleep(3000);}

public static void fitbaseAppDayLogSleepTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
		
	SleepPage Sleep = new SleepPage(driver);
	
	Sleep.FitbaseAppMenuIcon.click();
	Log.info("Click action performed on Menu icon");
			
	Thread.sleep(1500);
			
	Actions.menuPageScrollup(driver);
		
	Sleep.FitbaseAppSleepBtn.click();
	Log.info("Click Action Performed on Sleep Module Tab");  
		
	Thread.sleep(4000);
			
	Assert.assertTrue(Sleep.FitbaseAppDayLogSleepLink.isEnabled());	
	Sleep.FitbaseAppDayLogSleepLink.click();
	Log.info("Click action performed on Log Sleep Link");
		
	Sleep.FitbaseAppDayLogSleepPageSelectDate.click();
	Log.info("Click action performed on Cancel Icon Verfied");
			
	Sleep.FitbaseAppDayLogSleepPageSelectDateInPopupCalender.click();
	Log.info("Click action performed on calender date and date selected");
		
	Sleep.FitbaseAppDayLogSleepPageSelectDateInPopupCalenderOkButton.click();
	Log.info("Click action performed on Calender Ok Button");
		
	Sleep.FitbaseAppDayLogSleepPageLogBedTime.click();
	Log.info("Click action performed on Bed Time Verfied");
				
	Sleep.FitbaseAppDayLogSleepPageSelectBedTimeinPopupClock.click();
	Log.info("Click action performed on Time Picker and Bed Time selected");;
		
	Sleep.FitbaseAppDayLogSleepPageSelectBedTimeinPopupClockOkButton.click();
	Log.info("Click action performed on Date Picker Ok Button");
		
	Sleep.FitbaseAppDayLogSleepPageLogWakeUpTime.click();
	Log.info("Click action performed on select WakeUp Time Verfied");
		
	Sleep.FitbaseAppDayLogSleepPageSelectWakeUpTimeinPopupClock.click();
	Log.info("Click action performed on select Wakeup Time Verfied");
				
	Sleep.FitbaseAppDayLogSleepPageSelectWakeUpTimeinPopupClockOkButton.click();
	Log.info("Click action performed on selecting Wakeup Time Verfied");
				
	Sleep.FitbaseAppDayLogSleepPageSaveButton.click();
	Log.info("Click Action Performed on Save Button");
	Thread.sleep(5000);}

public static void fitbaseAppDaySleepSetReminderPageTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
		
	SleepPage Sleep = new SleepPage(driver);
	
	Actions.verifyMenuIcon(driver);
	Sleep.FitbaseAppMenuIcon.click();
	Log.info("Click action performed on Menu icon");
			
	Thread.sleep(1500);
			
	Actions.menuPageScrollup(driver);
		
	Assert.assertTrue(Sleep.FitbaseAppSleepBtn.isDisplayed());
	Sleep.FitbaseAppSleepBtn.click();
	Log.info("Click Action Performed on Sleep Button");
		
	Sleep.FitbaseAppDayLogSleepLink.click();
	Log.info("Click action performed on Add a Drink Link Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppDaySleepSetRemindericon.isDisplayed());
	Assert.assertTrue(Sleep.FitbaseAppDaySleepSetRemindericon.isEnabled());
	Log.info("Fitbase App Day Sleep Set Reminder icon Verfied");
		
	Sleep.FitbaseAppDaySleepSetRemindericon.click();
	Log.info("Click Action performed on Reminder icon Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderHeaderText.isDisplayed());
	Log.info("Fitbase App Day Sleep Page Set Reminder Header Text Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderBackArrowIcon.isDisplayed());
	Log.info("Fitbase App Day Sleep Page Set Reminder Back Arrow Icon Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderSleepReminderText.isDisplayed());
	Log.info("Fitbase App Day Sleep Page Set Reminder Sleep Reminder Text etc Verified");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderDoggleEnableReminderButton.isDisplayed());
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderDoggleEnableReminderButton.isEnabled());
	Log.info("Fitbase App Day Sleep Page Set Reminder Doggle Enable Reminder Button");
	
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderDoggleAlramSoundButton.isDisplayed());
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderDoggleAlramSoundButton.isEnabled());
	Log.info("Fitbase App Day Sleep Page Set Reminder Doggle Alram Sound Button");
	
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderAddIcon.isDisplayed());
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderAddIcon.isEnabled());
	Log.info("Fitbase App Day Sleep Page Set Reminder Add New Reminder Add Icon Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderTime.isDisplayed());
	Log.info("Fitbase App Day Sleep Page Set Reminder Add New Reminder Add Icon Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderTimeDogleButton.isDisplayed());
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderAddIcon.isEnabled());
	Log.info("Fitbase App Day Sleep Page Set Reminder Add New Reminder Add Icon Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderDaySelction.isDisplayed());
	Log.info("Fitbase App Day Sleep Page Set Reminder Add New Reminder Add Icon Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderTimeicon.isDisplayed());
	Log.info("Fitbase App Day Sleep Page Set Reminder Add New Reminder Add Icon Verfied");}
	
public static void fitbaseAppDaySleepSetReminderTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
		
	SleepPage Sleep = new SleepPage(driver);
		
	Actions.verifyMenuIcon(driver);
	Sleep.FitbaseAppMenuIcon.click();
	Log.info("Click action performed on Menu icon");
			
	Thread.sleep(1500);
			
	Actions.menuPageScrollup(driver);
		
	Assert.assertTrue(Sleep.FitbaseAppSleepBtn.isDisplayed());
	Sleep.FitbaseAppSleepBtn.click();
	Log.info("Click Action Performed on Sleep Button");
		
	Sleep.FitbaseAppDayLogSleepLink.click();
	Log.info("Click action performed on Log Sleep Link Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSleepReminderPopupPage.isDisplayed());
	Log.info("Fitbase App Day Sleep Page Set Reminder Add New Reminder Popup Sleep Reminder Popup Page Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSelectSleepReminderType.isDisplayed());
	Log.info("Fitbase App Day Sleep Page Set Reminder Add New Reminder Popup Select Sleep Reminder Type Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSelectSleepReminderSleepBox.isDisplayed());
	Log.info("Fitbase App Day Sleep Page Set Reminder Add New Reminder Popup Select Sleep Reminder Sleep Box Verfied");

	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSelectSleepReminderSleepRadioButton.isDisplayed());
	Log.info("Fitbase App Day Sleep Page Set Reminder Add New Reminder Popup Select Sleep Reminder Sleep RadioButton Verfied");

	Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSelectSleepReminderSleepRadioButton.click();
	Log.info("Fitbase App Day Set Reminder icon Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSelectSleepReminderWakeUpBox.isDisplayed());
	Log.info("Fitbase App Day Sleep Page Set Reminder Add New Reminder Popup Select Sleep Reminder WakeUp Box Verfied");

	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSelectSleepReminderWakeUpRadioButton.isDisplayed());
	Log.info("Fitbase App Day Sleep Page Set Reminder Add New Reminder Popup Select Sleep Reminder WakeUp RadioButton Verfied");

	Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSelectSleepReminderWakeUpRadioButton.click();
	Log.info("Fitbase App Day Set Reminder icon Verfied");

	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSleepReminderTime.isDisplayed());
	Log.info("Fitbase App Day Sleep Page Set Reminder Add New Reminder Popup Sleep Reminder Reminder Time Verfied");

	Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSelectSleepSelectReminderTime.click();
	Log.info("Fitbase App Day Set Reminder icon Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSleepRemindMeBefore.isDisplayed());
	Log.info("Fitbase App Day Sleep Page Set Reminder Add New Reminder Popup Sleep Reminder Reminder Time Verfied");
	
	Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSelectSleepRemindMeBeforeTime.click();
	Log.info("Fitbase App Day Set Reminder icon Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSleepRepeat.isDisplayed());
	Log.info("Fitbase App Day Sleep Page Set Reminder Add New Reminder Popup Sleep Reminder Reminder Time Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSleepRepeatAll.isDisplayed());
	Log.info("Fitbase App Day Sleep Page Set Reminder Add New Reminder Popup Sleep Reminder Reminder Time Verfied");
	
	Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSleepSelectDay.click();
	Log.info("Fitbase App Day Set Reminder icon Verfied");
	
	Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderSleepPopupCancelButton.click();
		
	Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderSleepPopupOkButton.click();
	Log.info("Fitbase App Day Set Reminder icon Verfied");
	
	Sleep.FitbaseAppDaySleepPageSetReminderAddNewReminderTimeDogleButton.click();}
	
public static void fitbaseAppDayTodaysRecentlyLoggedSleepPageTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
		
	SleepPage Sleep = new SleepPage(driver);
	
	Actions.menuPageScrollup(driver);
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepHeaderText.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep HeaderText Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepCloudMoonIcon.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Cloud Moon Icon Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepTimeRange.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Time Range Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepTime.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Time Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepThruDevices.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Thru Devices Verfied");}

public static void fitbaseAppDayEditLoggedSleepTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
		
	SleepPage Sleep = new SleepPage(driver);
		
	Sleep.FitbaseAppMenuIcon.click();
	Log.info("Click action performed on Menu icon");
			
	Thread.sleep(1500);
			
	Actions.menuPageScrollup(driver);
	
	Assert.assertTrue(Sleep.FitbaseAppSleepBtn.isDisplayed());
	Sleep.FitbaseAppSleepBtn.click();
	Log.info("Click Action Performed on Sleep Module Tab");  
		
	Thread.sleep(3000);
		
	Actions.menuPageScrollup(driver);
		
//	Actions.SleepTabSwipeLeft(driver);
	Log.info("Click action performed on Add a Drink Link Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepSwipe.isDisplayed());
	Log.info("Fitbas eApp Day Todays Recently Logged Glass Swipe Edit Icon Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEdit.isDisplayed());
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEdit.isEnabled());
	Log.info("Fitbase Ap pDay Todays Recently Logged Glass Swipe Delete icon Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepDelete.isDisplayed());
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepDelete.isEnabled());
	Log.info("Fitbase Ap pDay Todays Recently Logged Glass Swipe Delete icon Verfied");
		
	Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEdit.click();
	Log.info("Click action performed on Edit Button Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPage.isDisplayed());
	Log.info("Fitbase App Day Todays Recently Logged Glass Edit Drink Page Cancel Icon Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageBackArrowIcon.isDisplayed());
	Log.info("Fitbase App Day Todays Recently Logged Glass Edit Drink Page Cancel Icon Verfied");
			
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectDate.isDisplayed());
	Log.info("Click action performed on Page Log Time Date Verfied");
		
	Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectDate.click();
	Log.info("Click action performed on Log Time Verfied");
		
	Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectDateInPopupCalender.isDisplayed();
	Log.info("Fitbase App Drink Page Calender Verfied");
		
	Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectDateInPopupCalender.click();
	Log.info("Click action performed on calender date and date selected");
		
	Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectDateInPopupCalenderCancleButton.isDisplayed();
		
	Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectDateInPopupCalenderOkButton.isDisplayed();
	Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectDateInPopupCalenderOkButton.click();
	Log.info("Click action performed on Calender Ok Button");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageLogBedTime.isDisplayed());
	Log.info("Click action performed on Page Log Time Date Verfied");
		
	Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageLogBedTime.click();
	Log.info("Click action performed on Log Time Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectBedTimeinPopupClock.isDisplayed());
	Log.info("Click action performed on Page Log Time Date Verfied");
		
	Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectBedTimeinPopupClock.click();
	Log.info("Click action performed on Time Picker and Time selected");;
		
	Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectBedTimeinPopupClockOkButton.click();
	Log.info("Click action performed on Date Picker Ok Button");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageLogWakeUpTime.isDisplayed());
	Log.info("Click action performed on Page Select Drink Verfied");
		
	Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageLogWakeUpTime.click();
	Log.info("Click action performed on Page Select Drink Verfied");
					
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectWakeUpTimeinPopupClock.isDisplayed());
	Log.info("Fitbase App Add Drink Page select Drink in Popup 1Glass Verfied");
	
	Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectWakeUpTimeinPopupClock.click();
	Log.info("Click action performed on selecting 1 Glass Drink Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectWakeUpTimeinPopupClockOkButton.isDisplayed());
	Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectWakeUpTimeinPopupClockOkButton.click();
	Log.info("Fitbase App Add Drink Page select Drinki n Popup half Bottle Verfied");
				
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepDeviceIcon.isDisplayed());
	Log.info("Fitbase App Add Drink Page select Drink Popup Page Cancle Button Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepUpdateButton.isDisplayed());
	Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepUpdateButton.click();
	Log.info("Click action performed on Add Button Verfied");}

//---------------------------------------//WEEKLY--------------------------------------------
		
public static void fitbaseAppWeekSleepPageTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
		
	SleepPage Sleep = new SleepPage(driver);
	
	Sleep.FitbaseAppMenuIcon.click();
	Log.info("Click action performed on Menu icon");
			
	Thread.sleep(1500);
			
	Actions.menuPageScrollup(driver);
		
	Assert.assertTrue(Sleep.FitbaseAppSleepBtn.isDisplayed());
	Sleep.FitbaseAppSleepBtn.click();
	Log.info("Click Action Performed on Sleep Module Tab");  
	
	Thread.sleep(3000);
		    
	Assert.assertTrue(Sleep.FitbaseAppSleepHeadeText.isDisplayed());
	Log.info("Fitbase App Sleep Head Text Verfied");
				
	Assert.assertTrue(Sleep.FitbaseAppWeekTabText.isDisplayed());
	Assert.assertTrue(Sleep.FitbaseAppWeekTabText.isEnabled());
	Log.info("Fitbase App Day Sleep Tab Text Verfied");

	Sleep.FitbaseAppWeekTabText.click();
	
	Assert.assertTrue(Sleep.FitbaseAppWeekDate.isDisplayed());
	Log.info("Fitbase App Day Today Text Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppWeekForwardBackwardicon.isDisplayed());
	Assert.assertTrue(Sleep.FitbaseAppWeekForwardBackwardicon.isEnabled());
	Log.info("Fitbase App Day Sleep Today Forward Backward icons Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppWeekSleepPercentageEfficencyText.isDisplayed());
	Log.info("Fitbase App Day Sleep Time Recommended Text Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppWeekSleepCoudMoonIcon.isDisplayed());
	Log.info("Fitbase App Day Sleep Coud Moon Icon Verfied");

	Assert.assertTrue(Sleep.FitbaseAppWeekSleepDailyAverageTimeText.isDisplayed());
	Log.info("Fitbase App Day Sleep Total Slept Time Text Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppWeekSleepWeeklyAverageAwakeTimeText.isDisplayed());
	Log.info("Fitbase App Day Sleep Awake Text and Time Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepWeeklyAverageLightTextandTime.isDisplayed());
	Log.info("Fitbase App Day Sleep Light Text and Time Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppWeekSleepAverageDeepTextandTime.isDisplayed());
	Log.info("Fitbase App Day Sleep Deep Text and Time Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppWeekSleepGraph.isDisplayed());
	Log.info("Fitbase App Day Sleep Graph Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppWeekSleepMoonIconWithtime.isDisplayed());
	Log.info("Fitbase App Day Sleep Moon Icon With time Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepSunIconWithtime.isDisplayed());
	Log.info("Fitbase App Day Sleep Sun Icon With time Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppWeekSleepWakeLightDeepText.isDisplayed());
	Log.info("Fitbase App Day Sleep Wake Light Deep Text Verfied");
			
	Actions.menuPageScrollup(driver);
	
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepHeaderText.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Header Text Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepCloudMoonIcon.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Cloud Moon Icon Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepTimeRange.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Time Range Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepTime.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Time");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepThruDevices.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Thru Devices Verfied");
		
	Thread.sleep(3000);	}
		
//---------------------------------------//MONTHLY--------------------------------------------

	public static void fitbaseAppSleepMonthPageTestCase(AndroidDriver<AndroidElement> driver) throws Exception {

	SleepPage Sleep = new SleepPage(driver);
		
	Sleep.FitbaseAppMenuIcon.click();
	Log.info("Click action performed on Menu icon");
				
	Thread.sleep(1500);
				
	Actions.menuPageScrollup(driver);
			
	Assert.assertTrue(Sleep.FitbaseAppSleepBtn.isDisplayed());
	Sleep.FitbaseAppSleepBtn.click();
	Log.info("Click Action Performed on Sleep Module Tab");  
		
	Thread.sleep(3000);
			    
	Assert.assertTrue(Sleep.FitbaseAppSleepHeadeText.isDisplayed());
	Log.info("Fitbase App Sleep Head Text Verfied");
					
	Assert.assertTrue(Sleep.FitbaseAppMonthTabText.isDisplayed());
	Assert.assertTrue(Sleep.FitbaseAppMonthTabText.isEnabled());
	Log.info("Fitbase App Day Sleep Tab Text Verfied");

	Sleep.FitbaseAppMonthTabText.click();
		
	Assert.assertTrue(Sleep.FitbaseAppMonthText.isDisplayed());
	Log.info("Fitbase App Day Today Text Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppMonthForwardBackwardicon.isDisplayed());
	Assert.assertTrue(Sleep.FitbaseAppMonthForwardBackwardicon.isEnabled());
	Log.info("Fitbase App Day Sleep Today Forward Backward icons Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppMonthSleepTimeRecommendedText.isDisplayed());
	Log.info("Fitbase App Day Sleep Time Recommended Text Verfied");
			
	Assert.assertTrue(Sleep.FitbaseAppMonthSleepCoudMoonIcon.isDisplayed());
	Log.info("Fitbase App Day Sleep Coud Moon Icon Verfied");

	Assert.assertTrue(Sleep.FitbaseAppMonthSleepToatalSleptTimeText.isDisplayed());
	Log.info("Fitbase App Day Sleep Total Slept Time Text Verfied");
			
	Assert.assertTrue(Sleep.FitbaseAppMonthSleepAwakeTextandTime.isDisplayed());
	Log.info("Fitbase App Day Sleep Awake Text and Time Verfied");
			
	Assert.assertTrue(Sleep.FitbaseAppMonthSleepLightTextandTime.isDisplayed());
	Log.info("Fitbase App Day Sleep Light Text and Time Verfied");
			
	Assert.assertTrue(Sleep.FitbaseAppMonthSleepDeepTextandTime.isDisplayed());
	Log.info("Fitbase App Day Sleep Deep Text and Time Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppMonthSleepGraph.isDisplayed());
	Log.info("Fitbase App Day Sleep Graph Verfied");
			
	Assert.assertTrue(Sleep.FitbaseAppMonthSleepMoonIconWithtime.isDisplayed());
	Log.info("Fitbase App Day Sleep Moon Icon With time Verfied");
			
	Assert.assertTrue(Sleep.FitbaseAppMonthSleepSunIconWithtime.isDisplayed());
	Log.info("Fitbase App Day Sleep Sun Icon With time Verfied");
			
	Assert.assertTrue(Sleep.FitbaseAppMonthSleepWakeLightDeepText.isDisplayed());
	Log.info("Fitbase App Day Sleep Wake Light Deep Text Verfied");
				
	Actions.menuPageScrollup(driver);
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepHeaderText.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Header Text Verfied");
			
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepCloudMoonIcon.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Cloud Moon Icon Verfied");
			
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepTimeRange.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Time Range Verfied");
			
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepTime.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Time");
			
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepThruDevices.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Thru Devices Verfied");
			
	Thread.sleep(3000);	}
	
//---------------------------------------//YEARLY--------------------------------------------

public static void fitbaseAppSleepYearPageTestCase(AndroidDriver<AndroidElement> driver) throws Exception {

SleepPage Sleep = new SleepPage(driver);
	
	Sleep.FitbaseAppMenuIcon.click();
	Log.info("Click action performed on Menu icon");
			
	Thread.sleep(1500);
			
	Actions.menuPageScrollup(driver);
		
	Assert.assertTrue(Sleep.FitbaseAppSleepBtn.isDisplayed());
	Sleep.FitbaseAppSleepBtn.click();
	Log.info("Click Action Performed on Sleep Module Tab");  
	
	Thread.sleep(3000);
		    
	Assert.assertTrue(Sleep.FitbaseAppSleepHeadeText.isDisplayed());
	Log.info("Fitbase App Sleep Head Text Verfied");
				
	Assert.assertTrue(Sleep.FitbaseAppWeekTabText.isDisplayed());
	Assert.assertTrue(Sleep.FitbaseAppWeekTabText.isEnabled());
	Log.info("Fitbase App Day Sleep Tab Text Verfied");

	Sleep.FitbaseAppWeekTabText.click();
	
	Assert.assertTrue(Sleep.FitbaseAppWeekDate.isDisplayed());
	Log.info("Fitbase App Day Today Text Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppWeekForwardBackwardicon.isDisplayed());
	Assert.assertTrue(Sleep.FitbaseAppWeekForwardBackwardicon.isEnabled());
	Log.info("Fitbase App Day Sleep Today Forward Backward icons Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppWeekSleepPercentageEfficencyText.isDisplayed());
	Log.info("Fitbase App Day Sleep Time Recommended Text Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppWeekSleepCoudMoonIcon.isDisplayed());
	Log.info("Fitbase App Day Sleep Coud Moon Icon Verfied");

	Assert.assertTrue(Sleep.FitbaseAppWeekSleepDailyAverageTimeText.isDisplayed());
	Log.info("Fitbase App Day Sleep Total Slept Time Text Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppWeekSleepWeeklyAverageAwakeTimeText.isDisplayed());
	Log.info("Fitbase App Day Sleep Awake Text and Time Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepWeeklyAverageLightTextandTime.isDisplayed());
	Log.info("Fitbase App Day Sleep Light Text and Time Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppWeekSleepAverageDeepTextandTime.isDisplayed());
	Log.info("Fitbase App Day Sleep Deep Text and Time Verfied");
	
	Assert.assertTrue(Sleep.FitbaseAppWeekSleepGraph.isDisplayed());
	Log.info("Fitbase App Day Sleep Graph Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppWeekSleepMoonIconWithtime.isDisplayed());
	Log.info("Fitbase App Day Sleep Moon Icon With time Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepSunIconWithtime.isDisplayed());
	Log.info("Fitbase App Day Sleep Sun Icon With time Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppWeekSleepWakeLightDeepText.isDisplayed());
	Log.info("Fitbase App Day Sleep Wake Light Deep Text Verfied");
			
	Actions.menuPageScrollup(driver);
	
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepHeaderText.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Header Text Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepCloudMoonIcon.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Cloud Moon Icon Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepTimeRange.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Time Range Verfied");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepTime.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Time");
		
	Assert.assertTrue(Sleep.FitbaseAppDaySleepTodaysRecentlyLoggedSleepThruDevices.isDisplayed());
	Log.info("Fitbase App Day Sleep Todays Recently Logged Sleep Thru Devices Verfied");
		
	Thread.sleep(3000);	}

}

