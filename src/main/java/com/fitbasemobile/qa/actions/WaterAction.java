package com.fitbasemobile.qa.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import com.fitbasemobile.qa.pages.WaterPage;
import com.fitbasemobile.qa.utility.Log;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class WaterAction {

public static void fitbaseAppWaterDayPageTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
	
	WaterPage water = new WaterPage(driver);
	WebDriverWait wait = new WebDriverWait(driver ,50);
	
	wait.until(ExpectedConditions.elementToBeClickable(water.FitbaseAppMenuIcon)).click();
	Log.info("Click action performed on Menu icon");
	Thread.sleep(3000);
			
	water.FitbaseAppWaterTab.click();
	Log.info("Click Action Performed on Water Module Tab");  
	Thread.sleep(3000);
	    
	Assert.assertTrue(water.FitbaseAppWaterHeadeText.isDisplayed());
	Log.info("Fitbase App Water Head Text Verfied");
	
	Assert.assertTrue(water.FitbaseAppAddDrinkLink.isDisplayed());
	Log.info("Fitbase App Add Drink Link Verfied");
	
	try {
	if(water.FitbaseAppYouareconnectedtoDeviceSyncIcon.isDisplayed()== false) {
		Log.info("Fitbase App not connected to Device");
		}
	
	else{
		Log.info("Fitbase App You are connected to Device Sync icon Verfied ");
		}
}catch(Exception e) {
	e.getMessage();
}
	
	Assert.assertTrue(water.FitbaseAppDayTab.isDisplayed());
	Log.info("Fitbase App Day Tab Verfied");

	Assert.assertTrue(water.FitbaseAppDayTodayDatePicker.isDisplayed());
	Log.info("Fitbase App Day Today Date Picker Text Verfied");
		
	Assert.assertTrue(water.FitbaseAppDayRecommended8GlassesText.isDisplayed());
	Log.info("Fitbase App Day Recommended 8 Glasses Text Verfied");
	
	Assert.assertTrue(water.FitbaseAppDayGlassIcon.isDisplayed());
	Log.info("Fitbase App Day Glass Icon Verfied");
	
	Assert.assertTrue(water.FitbaseAppDayCirclepercentageIcon.isDisplayed());
	Log.info("Fitbase App Day Circle percentage Icon Verfied");

	Assert.assertTrue(water.FitbaseAppDayGoalGlassesTab.isDisplayed());
	Log.info("Fitbase App Day Goal Glasses Text Verfied");
	
	Assert.assertTrue(water.FitbaseAppDayGoalIntakeGlassesTab.isDisplayed());
	Log.info("Fitbase App Day Goal Intake Glasses Text Verfied");
	
	Assert.assertTrue(water.FitbaseAppDayGoalDrinkMoreTab.isDisplayed());
	Log.info("Fitbase App Day Goal Drink More Text Verfied");
		
	Assert.assertTrue(water.FitbaseAppDayAddaDrinkGlassIcon.isDisplayed()); 
	Log.info("Fitbase App Day Add a Drink Glass Icon Verfied");
	
	Assert.assertTrue(water.FitbaseAppDayAddaDrinkTextand250mlperglasstext.isDisplayed());
	Log.info("Fitbase App Day Add a Drink Text and 250ml per glass text Verfied");
	
	Actions.menuPageScrollup(driver);
	Thread.sleep(1500);
try {
	if(water.FitbaseAppDayTodaysLoggedHeaderText.isDisplayed()==true){
	Log.info("Fitbase App Day Todays Logged Header Text Verfied");
	
	Assert.assertTrue(water.FitbaseAppDayTodaysRecentlyLoggedGlassesHeaderText.isDisplayed());
	Log.info("Fitbase App Day Todays Recently Logged Glasses Header Text Verfied");
	
	Assert.assertTrue(water.FitbaseAppDayTodaysLoggedTextandTime.isDisplayed());
	Log.info("Fitbase App Day Todays Logged Text and Time Verfied");
	
	Assert.assertTrue(water.FitbaseAppDayTodaysGlassIcon.isDisplayed());
	Log.info("Fitbase App Day Todays Glass Icon Verfied");
	
	Actions.menuPageScrollup(driver);
	
	Assert.assertTrue(water.FitbaseAppDayTodaysLoggedGlassesMLText.isDisplayed());
	Log.info("Fitbase App Day Todays Logged Glasses ML Text Verfied");
	
	Assert.assertTrue(water.FitbaseAppDayTodaysLoggedNumberofGlasses.isDisplayed());
	Log.info("Fitbase App Day Todays Logged Number of Glasses Verfied");
	Thread.sleep(3000);
		}
	else{
		Log.info("No Drinks Added");
	}
}catch(Exception e) {
	e.getMessage();}
Thread.sleep(4000);
}	


public static void fitbaseAppAddaDrinkPageTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
	
	WaterPage water = new WaterPage(driver);
	WebDriverWait wait = new WebDriverWait(driver ,50);
	
	water.FitbaseAppMenuIcon.click();
	Log.info("Click action performed on Menu icon");
		
	Thread.sleep(1500);
			
	water.FitbaseAppWaterTab.click();
	Log.info("Click Action Performed on Water Module Tab");  
	
	Thread.sleep(1000);
	
	Assert.assertTrue(water.FitbaseAppAddDrinkLink.isDisplayed());
	Assert.assertTrue(water.FitbaseAppAddDrinkLink.isEnabled());
	Thread.sleep(1000);
	
	wait.until(ExpectedConditions.elementToBeClickable(water.FitbaseAppAddaDrinkLink)).click();
	Log.info("Click action performed on Add a Drink Link Verfied");
		
	Thread.sleep(3000);
	driver.findElement(By.xpath("\"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[2]/android.view.View\"")).isDisplayed();
Log.info("Add water verified");
	//	Assert.assertTrue(water.FitbaseAppAddDrinkPageHeaderText.isDisplayed());
//	Log.info("Fitbase App Add Drink Page Header Text Verfied");
//	
//	Assert.assertTrue(water.FitbaseAppAddDrinkPageBckArrow.isDisplayed());
//	Log.info("Fitbase App Add Drink Page Cancel Icon Verfied");
//	
//	Assert.assertTrue(water.FitbaseAppAddDrinkPageDate.isDisplayed());
//	Log.info("Fitbase App Add Drink Page Date Verfied");
//	
//	Assert.assertTrue(water.FitbaseAppAddDrinkPageLogTime.isDisplayed());
//	Log.info("Fitbase App Add Drink Page Log Time Verfied");
//
//	Assert.assertTrue(water.FitbaseAppAddDrinkPageSelectDrink.isDisplayed());
//	Log.info("Fitbase App Add Drink Page Select Drink Verfied");
//	
//	Assert.assertTrue(water.FitbaseAppAddDrinkPageSelectDrinkIcon.isDisplayed());
//	Log.info("Fitbase App Add Drink Page Select Drink Icon Verfied");
//		
//	Assert.assertTrue(water.FitbaseAppAddDrinkPageSaveButton.isDisplayed());
//	Log.info("Fitbase App Add Drink Page Save Button Verfied");
//	
//	water.FitbaseAppAddDrinkPageSaveButton.click();
//	Log.info("Click Action Performed on Save Button");  
}

public static void fitbaseAppAddDrinkTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
	
	WaterPage water = new WaterPage(driver);
	WebDriverWait wait = new WebDriverWait(driver ,50);
	
	wait.until(ExpectedConditions.elementToBeClickable(water.FitbaseAppMenuIcon)).click();
	Log.info("Click action performed on Menu icon");
	Thread.sleep(3000);
	
	Assert.assertTrue(water.FitbaseAppWaterTab.isDisplayed());
	water.FitbaseAppWaterTab.click();
	Log.info("Click Action Performed on Water Module Tab");  
	
	Thread.sleep(3000);
		
	Assert.assertTrue(water.FitbaseAppAddDrinkLink.isEnabled());	
	water.FitbaseAppAddaDrinkLink.click();
	Log.info("Click action performed on Add a Drink Link");
	
	Thread.sleep(3000);
	water.FitbaseAppAddDrinkPageSelectDate.click();
	Log.info("Click action performed on select date Verfied");
	
	driver.findElementByAccessibilityId("06 February 2020").click();
	Log.info("Click action performed on calender date and date selected");
	
	driver.findElementById("android:id/button1").click();
	Log.info("Click action performed on Calender Ok Button");
	Thread.sleep(2000);
	water.FitbaseAppAddDrinkPageSelectLogTime.click();
	Log.info("Click action performed on select Time Verfied");
	
	driver.findElementByAccessibilityId("2").click();
	Log.info("Click action performed on Log Time Verfied");
	
	driver.findElementById("android:id/button1").click();
	Log.info("Click action performed on Time Picker and Time selected");
	Thread.sleep(2000);

	water.FitbaseAppAddDrinkPageSaveButton.click();
	Log.info("Click Action Performed on Save Button");
	Thread.sleep(6000);}

//public static void fitbaseAppDaySetReminderPageTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
//	
//	WaterPage water = new WaterPage(driver);
//	
//	Actions.verifyMenuIcon(driver);
//	water.FitbaseAppMenuIcon.click();
//	Log.info("Click action performed on Menu icon");
//		
//	Thread.sleep(1500);
//		
//	Dimension size = driver.manage().window().getSize();
//		   
//	    Double scrollHeightStart = size.getHeight() * 0.8;
//	    int scrollStart = scrollHeightStart.intValue();
//	     
//	    Double scrollHeightEnd = size.getHeight() * 0.2;
//	    int scrollEnd = scrollHeightEnd.intValue();
//	                   
//	    TouchAction actions = new TouchAction(driver);
//	    actions.press(PointOption.point(0, scrollStart))
//	    .waitAction(waitOptions(Duration.ofSeconds(2)))
//	    .moveTo(PointOption.point(0, scrollEnd)).release().perform();
//	
//	Assert.assertTrue(water.FitbaseAppWaterBtn.isDisplayed());
//	water.FitbaseAppWaterBtn.click();
//	Log.info("Click Action Performed on Water Button");
//	
//	water.FitbaseAppAddaDrinkLink.click();
//	Log.info("Click action performed on Add a Drink Link Verfied");
//	
//	Assert.assertTrue(water.FitbaseAppDaySetRemindericon.isDisplayed());
//	Assert.assertTrue(water.FitbaseAppDaySetRemindericon.isEnabled());
//	Log.info("Fitbase App Day Set Reminder icon Verfied");
//	
//	water.FitbaseAppDaySetRemindericon.click();
//	Log.info("Fitbase App Day Set Reminder icon Verfied");
//
//	Assert.assertTrue(water.FitbaseAppDaySetReminderHeaderText.isDisplayed());
//	Log.info("Fitbase App Day Set Reminder icon Verfied");
//	
//	Assert.assertTrue(water.FitbaseAppDaySetReminderCancelIcon.isDisplayed());
//	Log.info("Fitbase App Day Set Reminder icon Verfied");
//	
//	Assert.assertTrue(water.FitbaseAppDaySetReminderWaterReminderText.isDisplayed());
//	Log.info("Fitbase App Day Set Reminder Water Reminders help you meet etc Text");
//	
//	Assert.assertTrue(water.FitbaseAppDaySetReminderDoggleButton.isDisplayed());
//	Assert.assertTrue(water.FitbaseAppDaySetReminderDoggleButton.isEnabled());
//	Log.info("Fitbase App Day Set Reminder icon Verfied");
//
//	Assert.assertTrue(water.FitbaseAppDaySetReminderAddNewReminderAddIcon.isDisplayed());
//	Assert.assertTrue(water.FitbaseAppDaySetReminderAddNewReminderAddIcon.isEnabled());
//	Log.info("Fitbase App Day Set Reminder icon Verfied");}
//
//public static void fitbaseAppDaySetReminderTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
//	
//	WaterPage water = new WaterPage(driver);
//	
//	Actions.verifyMenuIcon(driver);
//	water.FitbaseAppMenuIcon.click();
//	Log.info("Click action performed on Menu icon");
//		
//	Thread.sleep(1500);
//		
//	Dimension size = driver.manage().window().getSize();
//		   
//	    Double scrollHeightStart = size.getHeight() * 0.8;
//	    int scrollStart = scrollHeightStart.intValue();
//	     
//	    Double scrollHeightEnd = size.getHeight() * 0.2;
//	    int scrollEnd = scrollHeightEnd.intValue();
//	                   
//	    TouchAction actions = new TouchAction(driver);
//	    actions.press(PointOption.point(0, scrollStart))
//	    .waitAction(waitOptions(Duration.ofSeconds(2)))
//	    .moveTo(PointOption.point(0, scrollEnd)).release().perform();
//	
//	Assert.assertTrue(water.FitbaseAppWaterBtn.isDisplayed());
//	water.FitbaseAppWaterBtn.click();
//	Log.info("Click Action Performed on Water Button");
//	
//	water.FitbaseAppAddaDrinkLink.click();
//	Log.info("Click action performed on Add a Drink Link Verfied");
//	
//	Assert.assertTrue(water.FitbaseAppDaySetRemindericon.isDisplayed());
//	Assert.assertTrue(water.FitbaseAppDaySetRemindericon.isEnabled());
//	Log.info("Fitbase App Day Set Reminder icon Verfied");
//	
//	water.FitbaseAppDaySetRemindericon.click();
//	Log.info("Fitbase App Day Set Reminder icon Verfied");
//	
//	water.FitbaseAppDaySetReminderCancelIcon.click();
//	Log.info("Fitbase App Day Set Reminder icon Verfied");
//	
//	water.FitbaseAppDaySetRemindericon.click();
//	
//	Assert.assertTrue(water.FitbaseAppDaySetReminderDoggleButton.isSelected());
//	Log.info("Fitbase App Day Set Reminder icon Verfied");
//	
//	water.FitbaseAppDaySetReminderAddNewReminderAddIcon.click();
//	Log.info("Fitbase App Day Set Reminder icon Verfied");
//	
//	Assert.assertTrue(water.FitbaseAppDaySetReminderAddNewReminderSelectDrinkPopupPage.isDisplayed());
//	Log.info("Fitbase App Day Set Reminder icon Verfied");
//
//	Assert.assertTrue(water.FitbaseAppDaySetReminderAddNewReminderPopupSelectDrink.isSelected());
//	Log.info("Fitbase App Day Set Reminder icon Verfied");
//	
//	Assert.assertTrue(water.FitbaseAppDaySetReminderAddNewReminderPopupRepeatAll.isSelected());
//	Log.info("Fitbase App Day Set Reminder icon Verfied");
//	
//	Assert.assertTrue(water.FitbaseAppDaySetReminderAddNewReminderPopupCancelButton.isDisplayed());
//	Assert.assertTrue(water.FitbaseAppDaySetReminderAddNewReminderPopupCancelButton.isEnabled());
//	Log.info("Fitbase App Day Set Reminder icon Verfied");
//	
//	Assert.assertTrue(water.FitbaseAppDaySetReminderAddNewReminderPopupOkButton.isDisplayed());
//	Assert.assertTrue(water.FitbaseAppDaySetReminderAddNewReminderPopupOkButton.isEnabled());
//	Log.info("Fitbase App Day Set Reminder icon Verfied");}
//
public static void fitbaseAppDayAddaDrinkTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
	
	WaterPage water = new WaterPage(driver);
	
	water.FitbaseAppMenuIcon.click();
	Log.info("Click action performed on Menu icon");
		
	Thread.sleep(1500);
		
	Actions.menuPageScrollup(driver);
	
	Assert.assertTrue(water.FitbaseAppWaterTab.isDisplayed());
	water.FitbaseAppWaterTab.click();
	Log.info("Click Action Performed on Water Module Tab");  
	
	Thread.sleep(3000);
	
	Actions.menuPageScrollup(driver);
	
	water.FitbaseAppDayAddaDrinkIcon.click();
	Log.info("Click action performed on Add a Drink Link Verfied");
		
//	Assert.assertTrue(water.FitbaseAppDayAddaDrinkPopup.isDisplayed());
//	Log.info("Fitbase App Add Drink Page select Drink Popup Page Verfied");
//	
//	Assert.assertTrue(water.FitbaseAppDayAddaDrinkPopupSelectDrink.isDisplayed());
//	Log.info("Fitbase App Add Drink Page select Drink Popup Page Select Glass or Bottle Verfied");
//	
//	Assert.assertTrue(water.FitbaseAppDayAddaDrinkPopupCancelButton.isDisplayed());
//	Log.info("Fitbase App Add Drink Page select Drink Popup Page Cancle Button Verfied");
//	
//	Assert.assertTrue(water.FitbaseAppDayAddaDrinkPopupAddButton.isDisplayed());
//	Log.info("Fitbase App Add Drink Page select Drink Popup Page Select Button Verfied");
//	
//	water.FitbaseAppDayAddaDrinkPopupAddButton.click();
//	Log.info("Click action performed on Select Button Verfied");
	}
//
public static void fitbaseAppDayTodaysRecentlyLoggedGlassesPageTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
	
	WaterPage water = new WaterPage(driver);
	
	Actions.menuPageScrollup(driver);
	
	Assert.assertTrue(water.FitbaseAppDayTodaysRecentlyLoggedGlassesHeaderText.isDisplayed());
	Log.info("Fitbase App Day Todays Recently Logged Glasses Header Text Verfied");
	
	Assert.assertTrue(water.FitbaseAppDayTodaysLoggedTextandTime.isDisplayed());
	Log.info("Fitbase App Day Todays Logged Text and Time Verfied");
	
	Assert.assertTrue(water.FitbaseAppDayTodaysGlassIcon.isDisplayed());
	Log.info("Fitbase App Day Todays Glass Icon Verfied");
	
	Assert.assertTrue(water.FitbaseAppDayTodaysLoggedGlassesMLText.isDisplayed());
	Log.info("Fitbase App Day Todays Logged Glasses ML Text Verfied");
	
	Assert.assertTrue(water.FitbaseAppDayTodaysLoggedNumberofGlasses.isDisplayed());
	Log.info("Fitbase App Day Todays Logged Number of Glasses Verfied");
	
//	Assert.assertTrue(water.FitbaseAppDayTodaysLoggedNumberofGlassesThruDevices.isDisplayed());
//	Log.info("Fitbase App Day Set Reminder icon Verfied");
	}

public static void fitbaseAppDayEditDrinkTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
	
	WaterPage water = new WaterPage(driver);
	
	water.FitbaseAppMenuIcon.click();
	Log.info("Click action performed on Menu icon");
		
	Thread.sleep(1500);
		
	Actions.menuPageScrollup(driver);
	
	Assert.assertTrue(water.FitbaseAppWaterTab.isDisplayed());
	water.FitbaseAppWaterTab.click();
	Log.info("Click Action Performed on Water Module Tab");  
	
	Thread.sleep(3000);
	
	Actions.menuPageScrollup(driver);
	
	Actions.waterTabSwipeLeft(driver);
	Log.info("Click action performed on Add a Drink Link Verfied");
	
	Assert.assertTrue(water.FitbaseAppDayTodaysRecentlyLoggedGlassSwipe.isDisplayed());
	Assert.assertTrue(water.FitbaseAppDayTodaysRecentlyLoggedGlassSwipe.isEnabled());
	Log.info("Fitbas eApp Day Todays Recently Logged Glass Swipe Edit Icon Verfied");
	
	Assert.assertTrue(water.FitbaseAppDayTodaysRecentlyLoggedGlassSwipeDelete.isDisplayed());
	Assert.assertTrue(water.FitbaseAppDayTodaysRecentlyLoggedGlassSwipeDelete.isEnabled());
	Log.info("Fitbase Ap pDay Todays Recently Logged Glass Swipe Delete icon Verfied");
	
	water.FitbaseAppDayTodaysRecentlyLoggedGlassSwipeEdit.click();
	Log.info("Click action performed on Edit Button Verfied");
	
//	water.FitbaseAppDayTodaysRecentlyLoggedGlassEditDrinkPage.isDisplayed());
//	Log.info("Click action performed on Edit Page Verfied");
	
	Assert.assertTrue(water.FitbaseAppDayTodaysRecentlyLoggedGlassEditDrinkPageCancelIcon.isDisplayed());
	Log.info("Fitbase App Day Todays Recently Logged Glass Edit Drink Page Cancel Icon Verfied");
		
	Assert.assertTrue(water.FitbaseAppDayTodaysRecentlyLoggedGlassEditDrinkPageDate.isDisplayed());
	Log.info("Click action performed on Page Log Time Date Verfied");
	
	water.FitbaseAppDayTodaysRecentlyLoggedGlassEditDrinkPageDate.click();
	Log.info("Click action performed on Log Time Verfied");
	
	water.FitbaseAppAddDrinkPageSelectDateInPopupCalender.isDisplayed();
	Log.info("Fitbase App Drink Page Calender Verfied");
	
	water.FitbaseAppAddDrinkPageSelectDateInPopupCalender.click();
	Log.info("Click action performed on calender date and date selected");
	
	water.FitbaseAppAddDrinkPageSelectDateInPopupCalenderCancleButton.isDisplayed();
	
	water.FitbaseAppAddDrinkPageSelectDateInPopupCalenderOkButton.isDisplayed();
	water.FitbaseAppAddDrinkPageSelectDateInPopupCalenderOkButton.click();
	Log.info("Click action performed on Calender Ok Button");
	
	Assert.assertTrue(water.FitbaseAppDayTodaysRecentlyLoggedGlassEditDrinkPageLogTime.isDisplayed());
	Log.info("Click action performed on Page Log Time Date Verfied");
	
	water.FitbaseAppDayTodaysRecentlyLoggedGlassEditDrinkPageLogTime.click();
	Log.info("Click action performed on Log Time Verfied");
	
	water.FitbaseAppAddDrinkPageSelectLogTimeinPopupClock.isDisplayed();
	Log.info("Click action performed on Log Time Verfied");
	
	water.FitbaseAppAddDrinkPageSelectLogTimeinPopupClock.click();
	Log.info("Click action performed on Time Picker and Time selected");;
	
	water.FitbaseAppAddDrinkPageSelectLogTimeinPopupClockOkButton.isDisplayed();
	water.FitbaseAppAddDrinkPageSelectLogTimeinPopupClockOkButton.click();
	Log.info("Click action performed on Date Picker Ok Button");
	
	Assert.assertTrue(water.FitbaseAppDayTodaysRecentlyLoggedGlassEditDrinkPageSelectDrink.isDisplayed());
	Log.info("Click action performed on Page Select Drink Verfied");
	
	water.FitbaseAppDayTodaysRecentlyLoggedGlassEditDrinkPageSelectDrink.click();
	Log.info("Click action performed on Page Select Drink Verfied");
	
	water.FitbaseAppAddDrinkPageselectDrinkPopup.isDisplayed();
	Log.info("Click action performed on select Drink Verfied");
		
	Assert.assertTrue(water.FitbaseAppAddDrinkPageselectDrinkinPopup1Glass.isDisplayed());
	Log.info("Fitbase App Add Drink Page select Drink in Popup 1Glass Verfied");
	
	water.FitbaseAppAddDrinkPageselectDrinkinPopup1Glass.click();
	Log.info("Click action performed on selecting 1 Glass Drink Verfied");
	
//	Assert.assertTrue(water.FitbaseAppAddDrinkPageselectDrinkinPopuphalfBottle.isDisplayed());
//	Log.info("Fitbase App Add Drink Page select Drinki n Popup half Bottle Verfied");
	
//	Assert.assertTrue(water.FitbaseAppAddDrinkPageselectDrinkinPopup1Bottle.isDisplayed());
//	Log.info("Fitbase App Add Drink Page select Drinki n Popup half Bottle Verfied");
	
	Assert.assertTrue(water.FitbaseAppAddDrinkPageselectDrinkPopupPageCancleButton.isDisplayed());
	water.FitbaseAppAddDrinkPageselectDrinkPopupPageCancleButton.click();
	Log.info("Fitbase App Add Drink Page select Drink Popup Page Cancle Button Verfied");
	
	Assert.assertTrue(water.FitbaseAppAddDrinkPageselectDrinkPopupPageAddButton.isDisplayed());
//	water.FitbaseAppAddDrinkPageselectDrinkPopupPageAddButton.click();
	Log.info("Click action performed on Add Button Verfied");
	
	water.FitbaseAppAddDrinkPageSaveButton.click();
	Log.info("Click Action Performed on Save Button");
	Thread.sleep(5000);}

//---------------------------------------//WEEKLY--------------------------------------------
	
public static void fitbaseAppWaterWeekPageTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
	
	WaterPage water = new WaterPage(driver);
	
	water.FitbaseAppMenuIcon.click();
	Log.info("Click action performed on Menu icon ");
		
	Thread.sleep(1500);
		
	Actions.menuPageScrollup(driver);
	
	Assert.assertTrue(water.FitbaseAppWaterTab.isDisplayed());
	water.FitbaseAppWaterTab.click();
	Log.info("Click Action Performed on Water Module Tab verified");  
	
	Thread.sleep(3000);
	    
	Assert.assertTrue(water.FitbaseAppWeekTabText.isDisplayed());
	Log.info("Fitbase App Week Tab Text Verfied");
	
	water.FitbaseAppWeekTabText.click();
	Log.info("Click Action Performed on Week Tab Verified"); 
		
	Assert.assertTrue(water.FitbaseAppWeekDate.isDisplayed());
	Log.info("Fitbase App Week Date Verfied");
	
	//Assert.assertTrue(water.FitbaseAppWeekBackwardicons.isDisplayed());
    //Assert.assertTrue(water.FitbaseAppWeekBackwardicons.isEnabled());
	//Log.info("Fitbase App Week Backward icons Verfied");
	
	//Assert.assertTrue(water.FitbaseAppWeekForwardicons.isDisplayed());
    //Assert.assertTrue(water.FitbaseAppWeekForwardicons.isEnabled());
	//Log.info("Fitbase App Week Forward icons Verfied");
	
	water.FitbaseAppDayTodaysRecentlyLoggedGlassEditDrinkPageDate.click();
	Log.info("Click action performed on Log Time Verfied");
		
	water.FitbaseAppAddDrinkPageSelectDateInPopupCalender.click();
	Log.info("Click action performed on calender date and date selected");
		
	water.FitbaseAppAddDrinkPageSelectDateInPopupCalenderOkButton.click();
	Log.info("Click action performed on Calender Ok Button");
	
	Log.info("Click action performed on Page Log Time Date Verfied");
	
	water.FitbaseAppDayTodaysRecentlyLoggedGlassEditDrinkPageLogTime.click();
	Log.info("Click action performed on Log Time Verfied");
		
	water.FitbaseAppAddDrinkPageSelectLogTimeinPopupClock.click();
	Log.info("Click action performed on Time Picker and Time selected");;
	
	water.FitbaseAppAddDrinkPageSelectLogTimeinPopupClockOkButton.click();
	Log.info("Click action performed on Date Picker Ok Button");
		
	water.FitbaseAppDayTodaysRecentlyLoggedGlassEditDrinkPageSelectDrink.click();
	Log.info("Click action performed on Page Select Drink Verfied");
			
	water.FitbaseAppAddDrinkPageselectDrinkinPopup1Glass.click();
	Log.info("Click action performed on selecting 1 Glass Drink Verfied");
	
	water.FitbaseAppAddDrinkPageselectDrinkPopupPageCancleButton.click();
	Log.info("Fitbase App Add Drink Page select Drink Popup Page Cancle Button Verfied");

//	water.FitbaseAppAddDrinkPageselectDrinkPopupPageAddButton.click();
	Log.info("Click action performed on Add Button Verfied");
	
	water.FitbaseAppAddDrinkPageSaveButton.click();
	Log.info("Click Action Performed on Save Button");
	Thread.sleep(5000);

	Actions.menuPageScrollup(driver);
	
//	Assert.assertTrue(water.FitbaseAppDayTodaysLoggedNumberofGlassesThruDevices.isDisplayed());
//	Log.info("Fitbase App Week Set Reminder icon Verfied");
	
	Thread.sleep(3000);	}

public static void fitbaseAppWaterWeekTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
	
	WaterPage water = new WaterPage(driver);
	
	water.FitbaseAppMenuIcon.click();
	Log.info("Click action performed on Menu icon ");
		
	Thread.sleep(1500);
		
	Actions.menuPageScrollup(driver);
	
	Assert.assertTrue(water.FitbaseAppWaterTab.isDisplayed());
	water.FitbaseAppWaterTab.click();
	Log.info("Click Action Performed on Water Module Tab verified");  
	
	Thread.sleep(3000);
	
	water.FitbaseAppWeekTabText.click();
	Log.info("Click Action Performed on Week Tab Verified"); 
	
	Assert.assertTrue(water.FitbaseAppAddDrinkLink.isEnabled());	
	water.FitbaseAppAddaDrinkLink.click();
	Log.info("Click action performed on Add a Drink Link");
	
	water.FitbaseAppAddDrinkPageSelectDate.click();
	Log.info("Click action performed on Cancel Icon Verfied");
	
	Log.info("Fitbase App Drink Page Calender Verfied");
	
	water.FitbaseAppAddDrinkPageSelectDateInPopupCalender.click();
	Log.info("Click action performed on calender date and date selected");
	
	water.FitbaseAppAddDrinkPageSelectDateInPopupCalenderOkButton.click();
	Log.info("Click action performed on Calender Ok Button");
	
	water.FitbaseAppAddDrinkPageSelectLogTime.click();
	Log.info("Click action performed on Log Time Verfied");
	
	water.FitbaseAppAddDrinkPageSelectLogTimeinPopupClock.click();
	Log.info("Click action performed on Time Picker and Time selected");;
	
	water.FitbaseAppAddDrinkPageSelectLogTimeinPopupClockOkButton.click();
	Log.info("Click action performed on Date Picker Ok Button");
	
	water.FitbaseAppAddDrinkPageselectDrink.click();
	Log.info("Click action performed on select Drink Verfied");
	
	water.FitbaseAppAddDrinkPageselectDrinkinPopup1Glass.click();
	Log.info("Click action performed on selecting 1 Glass Drink Verfied");
	
	water.FitbaseAppAddDrinkPageselectDrinkPopupPageCancleButton.click();
	Log.info("Fitbase App Add Drink Page select Drink Popup Page Cancle Button Verfied");
	
//	water.FitbaseAppAddDrinkPageselectDrinkPopupPageAddButton.click();
	Log.info("Click action performed on Add Button Verfied");
	
	water.FitbaseAppAddDrinkPageSaveButton.click();
	Log.info("Click Action Performed on Save Button");
	Thread.sleep(5000);
		
    //Assert.assertTrue(water.FitbaseAppWeekBackwardicons.isEnabled());
	//Log.info("Fitbase App Week Backward icons Verfied");
	
    //Assert.assertTrue(water.FitbaseAppWeekForwardicons.isEnabled());
	//Log.info("Fitbase App Week Forward icons Verfied");
	
	Actions.menuPageScrollup(driver);
		
	Thread.sleep(3000);	}
		
//---------------------------------------//MONTHLY--------------------------------------------

public static void fitbaseAppWaterMonthPageTestCase(AndroidDriver<AndroidElement> driver) throws Exception {

	WaterPage water = new WaterPage(driver);

	water.FitbaseAppMenuIcon.click();
	Log.info("Click action performed on Menu icon ");

	Thread.sleep(1500);

	Actions.menuPageScrollup(driver);

	Assert.assertTrue(water.FitbaseAppWaterTab.isDisplayed());
	water.FitbaseAppWaterTab.click();
	Log.info("Click Action Performed on Water Module Tab verified");  

	Thread.sleep(3000);

	Assert.assertTrue(water.FitbaseAppMonthTabText.isDisplayed());
	Log.info("Fitbase App Month Tab Text Verfied");

	water.FitbaseAppMonthTabText.click();
	Log.info("Click Action Performed on Month Tab Verified"); 

	Assert.assertTrue(water.FitbaseAppMonthDate.isDisplayed());
	Log.info("Fitbase App Month Date Verfied");

//Assert.assertTrue(water.FitbaseAppMonthBackwardicons.isDisplayed());
//Assert.assertTrue(water.FitbaseAppMonthBackwardicons.isEnabled());
//Log.info("Fitbase App Month Backward icons Verfied");

//Assert.assertTrue(water.FitbaseAppMonthForwardicons.isDisplayed());
//Assert.assertTrue(water.FitbaseAppMonthForwardicons.isEnabled());
//Log.info("Fitbase App Month Forward icons Verfied");

	Assert.assertTrue(water.FitbaseAppMonthAveragemlperdayText.isDisplayed());
	Log.info("Fitbase App Month Average ml per day Text Verfied");

	Assert.assertTrue(water.FitbaseAppMonthGlassIcon.isDisplayed());
	Log.info("Fitbase App Month Glass Icon Verfied");

	Assert.assertTrue(water.FitbaseAppMonthDrinkFrequencyTimesperdayText.isDisplayed());
	Log.info("Fitbase App Month Drink Frequency Times per day Text Verfied");

	Assert.assertTrue(water.FitbaseAppMonthGraph.isDisplayed());
	Log.info("Fitbase App Month Graph Verfied");

	Assert.assertTrue(water.FitbaseAppMonthCupIcon.isDisplayed());
	Log.info("Fitbase App Month Cup Icon Verfied");

	Assert.assertTrue(water.FitbaseAppMonthyouhavedrankGlassesofWaterThisMonthText.isDisplayed());
	Log.info("Fitbase App Month you have drank Glasses of Water This Month Text Verfied");

	Assert.assertTrue(water.FitbaseAppThisMonthRecentlyThisMonthLoggedGlassesText.isDisplayed());
	Log.info("Fitbase App This Month Recently This Month Logged Glasses Text Verfied");

	Actions.menuPageScrollup(driver);

	Assert.assertTrue(water.FitbaseAppThisMonthLoggedGlassesIcon.isDisplayed());
	Log.info("Fitbase App This Month Logged Glasses Icon Verfied");

	Assert.assertTrue(water.FitbaseAppMonthLoggedTextandTime.isDisplayed());
	Log.info("Fitbase App Month Logged Text and Time Verfied");

	Assert.assertTrue(water.FitbaseAppMonthLoggedGlassesMLText.isDisplayed());
	Log.info("Fitbase App Month Logged Glasses ML Text");

	Assert.assertTrue(water.FitbaseAppMonthLoggedNumberofGlasses.isDisplayed());
	Log.info("Fitbase App Month Logged Number of Glasses Verfied");

//Assert.assertTrue(water.FitbaseAppDayTodaysLoggedNumberofGlassesThruDevices.isDisplayed());
//Log.info("Fitbase App Month Set Reminder icon Verfied");

	Thread.sleep(3000);	}

public static void fitbaseAppWaterMonthTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
	
	WaterPage water = new WaterPage(driver);
	
	water.FitbaseAppMenuIcon.click();
	Log.info("Click action performed on Menu icon ");
		
	Thread.sleep(1500);
		
	Actions.menuPageScrollup(driver);
	
	Assert.assertTrue(water.FitbaseAppWaterTab.isDisplayed());
	water.FitbaseAppWaterTab.click();
	Log.info("Click Action Performed on Water Module Tab verified");  
	
	Thread.sleep(3000);
	
	water.FitbaseAppMonthTabText.click();
	Log.info("Click Action Performed on Week Tab Verified"); 
	
	Assert.assertTrue(water.FitbaseAppAddDrinkLink.isEnabled());	
	water.FitbaseAppAddaDrinkLink.click();
	Log.info("Click action performed on Add a Drink Link");
	
	water.FitbaseAppAddDrinkPageSelectDate.click();
	Log.info("Click action performed on Cancel Icon Verfied");
	
	Log.info("Fitbase App Drink Page Calender Verfied");
	
	water.FitbaseAppAddDrinkPageSelectDateInPopupCalender.click();
	Log.info("Click action performed on calender date and date selected");
	
	water.FitbaseAppAddDrinkPageSelectDateInPopupCalenderOkButton.click();
	Log.info("Click action performed on Calender Ok Button");
	
	water.FitbaseAppAddDrinkPageSelectLogTime.click();
	Log.info("Click action performed on Log Time Verfied");
	
	water.FitbaseAppAddDrinkPageSelectLogTimeinPopupClock.click();
	Log.info("Click action performed on Time Picker and Time selected");;
	
	water.FitbaseAppAddDrinkPageSelectLogTimeinPopupClockOkButton.click();
	Log.info("Click action performed on Date Picker Ok Button");
	
	water.FitbaseAppAddDrinkPageselectDrink.click();
	Log.info("Click action performed on select Drink Verfied");
	
	water.FitbaseAppAddDrinkPageselectDrinkinPopup1Glass.click();
	Log.info("Click action performed on selecting 1 Glass Drink Verfied");
	
	water.FitbaseAppAddDrinkPageselectDrinkPopupPageCancleButton.click();
	Log.info("Fitbase App Add Drink Page select Drink Popup Page Cancle Button Verfied");
	
//	water.FitbaseAppAddDrinkPageselectDrinkPopupPageAddButton.click();
	Log.info("Click action performed on Add Button Verfied");
	
	water.FitbaseAppAddDrinkPageSaveButton.click();
	Log.info("Click Action Performed on Save Button");
	Thread.sleep(5000);
		
    //Assert.assertTrue(water.FitbaseAppMonthBackwardicons.isEnabled());
	//Log.info("Fitbase App Month Backward icons Verfied");
	
    //Assert.assertTrue(water.FitbaseAppMonthForwardicons.isEnabled());
	//Log.info("Fitbase App Month Forward icons Verfied");
	
	Actions.menuPageScrollup(driver);
		
	Thread.sleep(3000);	}

//---------------------------------------//YEARLY--------------------------------------------

public static void fitbaseAppWaterYearPageTestCase(AndroidDriver<AndroidElement> driver) throws Exception {

	WaterPage water = new WaterPage(driver);

	water.FitbaseAppMenuIcon.click();
	Log.info("Click action performed on Menu icon ");

	Thread.sleep(1500);

	Actions.menuPageScrollup(driver);

	Assert.assertTrue(water.FitbaseAppWaterTab.isDisplayed());
	water.FitbaseAppWaterTab.click();
	Log.info("Click Action Performed on Water Module Tab verified");  

	Thread.sleep(3000);

	Assert.assertTrue(water.FitbaseAppYearTabText.isDisplayed());
	Log.info("Fitbase App Year Tab Text Verfied");

	water.FitbaseAppYearTabText.click();
	Log.info("Click Action Performed on Year Tab Verified"); 

	Assert.assertTrue(water.FitbaseAppYearDate.isDisplayed());
	Log.info("Fitbase App Year Date Verfied");

	//Assert.assertTrue(water.FitbaseAppYearBackwardicons.isDisplayed());
	//Assert.assertTrue(water.FitbaseAppYearBackwardicons.isEnabled());
	//Log.info("Fitbase App Year Backward icons Verfied");

	//Assert.assertTrue(water.FitbaseAppYearForwardicons.isDisplayed());
	//Assert.assertTrue(water.FitbaseAppYearForwardicons.isEnabled());
	//Log.info("Fitbase App Year Forward icons Verfied");

	Assert.assertTrue(water.FitbaseAppYearAveragemlperdayText.isDisplayed());
	Log.info("Fitbase App Month Average ml per day Text Verfied");

	Assert.assertTrue(water.FitbaseAppYearGlassIcon.isDisplayed());
	Log.info("Fitbase App Month Glass Icon Verfied");

	Assert.assertTrue(water.FitbaseAppYearDrinkFrequencyTimesperdayText.isDisplayed());
	Log.info("Fitbase App Month Drink Frequency Times per day Text Verfied");

	Assert.assertTrue(water.FitbaseAppYearGraph.isDisplayed());
	Log.info("Fitbase App Month Graph Verfied");

	Assert.assertTrue(water.FitbaseAppYearCupIcon.isDisplayed());
	Log.info("Fitbase App Month Cup Icon Verfied");

	Assert.assertTrue(water.FitbaseAppYearyouhavedrankGlassesofWaterThisYearText.isDisplayed());
	Log.info("Fitbase App Year you have drank Glasses of Water This Year Text Verfied");

	Assert.assertTrue(water.FitbaseAppThisYearRecentlyThisYearLoggedGlassesText.isDisplayed());
	Log.info("Fitbase App This Year Recently This Year Logged Glasses Text Verfied");

	Actions.menuPageScrollup(driver);
	
	Assert.assertTrue(water.FitbaseAppThisYearLoggedGlassesIcon.isDisplayed());
	Log.info("Fitbase App This Year Logged Glasses Icon Verfied");

	Assert.assertTrue(water.FitbaseAppYearLoggedTextandTime.isDisplayed());
	Log.info("Fitbase App Year Logged Text and Time Verfied");

	Assert.assertTrue(water.FitbaseAppYearLoggedGlassesMLText.isDisplayed());
	Log.info("Fitbase App Year Logged Glasses ML Text");

	Assert.assertTrue(water.FitbaseAppYearLoggedNumberofGlasses.isDisplayed());
	Log.info("Fitbase App Year Logged Number of Glasses Verfied");

	//Assert.assertTrue(water.FitbaseAppDayTodaysLoggedNumberofGlassesThruDevices.isDisplayed());
	//Log.info("Fitbase App Month Set Reminder icon Verfied");

	Thread.sleep(3000);	}

public static void fitbaseAppWaterYearTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
	
	WaterPage water = new WaterPage(driver);
	
	water.FitbaseAppMenuIcon.click();
	Log.info("Click action performed on Menu icon ");
		
	Thread.sleep(1500);
		
	Actions.menuPageScrollup(driver);
	
	Assert.assertTrue(water.FitbaseAppWaterTab.isDisplayed());
	water.FitbaseAppWaterTab.click();
	Log.info("Click Action Performed on Water Module Tab verified");  
	
	Thread.sleep(3000);
	
	water.FitbaseAppYearTabText.click();
	Log.info("Click Action Performed on Year Tab Verified"); 
	
	Assert.assertTrue(water.FitbaseAppAddDrinkLink.isEnabled());	
	water.FitbaseAppAddaDrinkLink.click();
	Log.info("Click action performed on Add a Drink Link");
	
	water.FitbaseAppAddDrinkPageSelectDate.click();
	Log.info("Click action performed on Cancel Icon Verfied");
	
	Log.info("Fitbase App Drink Page Calender Verfied");
	
	water.FitbaseAppAddDrinkPageSelectDateInPopupCalender.click();
	Log.info("Click action performed on calender date and date selected");
	
	water.FitbaseAppAddDrinkPageSelectDateInPopupCalenderOkButton.click();
	Log.info("Click action performed on Calender Ok Button");
	
	water.FitbaseAppAddDrinkPageSelectLogTime.click();
	Log.info("Click action performed on Log Time Verfied");
	
	water.FitbaseAppAddDrinkPageSelectLogTimeinPopupClock.click();
	Log.info("Click action performed on Time Picker and Time selected");;
	
	water.FitbaseAppAddDrinkPageSelectLogTimeinPopupClockOkButton.click();
	Log.info("Click action performed on Date Picker Ok Button");
	
	water.FitbaseAppAddDrinkPageselectDrink.click();
	Log.info("Click action performed on select Drink Verfied");
	
	water.FitbaseAppAddDrinkPageselectDrinkinPopup1Glass.click();
	Log.info("Click action performed on selecting 1 Glass Drink Verfied");
	
	water.FitbaseAppAddDrinkPageselectDrinkPopupPageCancleButton.click();
	Log.info("Fitbase App Add Drink Page select Drink Popup Page Cancle Button Verfied");
	
	water.FitbaseAppAddDrinkPageselectDrinkPopupPageAddButton.click();
	Log.info("Click action performed on Add Button Verfied");
	
	water.FitbaseAppAddDrinkPageSaveButton.click();
	Log.info("Click Action Performed on Save Button");
	Thread.sleep(5000);
		
    //Assert.assertTrue(water.FitbaseAppYearBackwardicons.isEnabled());
	//Log.info("Fitbase App Year Backward icons Verfied");
	
    //Assert.assertTrue(water.FitbaseAppYearForwardicons.isEnabled());
	//Log.info("Fitbase App Year Forward icons Verfied");
	
	Actions.menuPageScrollup(driver);
		
	Thread.sleep(3000);	}
}
