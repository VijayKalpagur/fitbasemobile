package com.fitbasemobile.qa.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.fitbasemobile.qa.pages.DevicesPage;
import com.fitbasemobile.qa.pages.LoginPage;
import com.fitbasemobile.qa.utility.Log;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class DevicesAction {

public static void fitbaseAppDevicesPageTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
		
	DevicesPage device = new DevicesPage(driver);
		
	device.FitbaseAppMenuIcon.click();
	Log.info("Click action performed on Menu icon");
			
	Thread.sleep(1500);
			
	Actions.menuPageScrollup(driver);
		
	Assert.assertTrue(device.FitbaseAppDevicesModuleButton.isDisplayed());
	device.FitbaseAppDevicesModuleButton.click();
	Log.info("Click Action Performed on Device Module Tab");  
		
	Thread.sleep(3000);
		    
	Assert.assertTrue(device.FitbaseAppDevicesHeadeText.isDisplayed());
	Log.info("Fitbase App devices Head Text Verfied");
				
	Assert.assertTrue(device.FitbaseAppDeviceIcon.isDisplayed());
	Log.info("Fitbase App Device Icon Verfied");
		
	Assert.assertTrue(device.FitbaseAppFitbitDeviceNoDeviceConnectedText.isDisplayed());
	Log.info("Fitbase App Device No Device Connected Text Verfied");

	Assert.assertTrue(device.FitbaseAppAvailableDevicesHeaderText.isDisplayed());
	Log.info("Fitbase App Available Devices Header Text Verfied");
		
	Assert.assertTrue(device.FitbaseAppFitbitDeviceIcon.isDisplayed());
	Log.info("Fitbase App Fitbit Device Icon Verfied");
		
	Assert.assertTrue(device.FitbaseAppFitbitDeviceNameText.isDisplayed());
	Log.info("Fitbase App Fitbit Device Name Text Verfied");
		
	Assert.assertTrue(device.FitbaseAppFitbitDeviceConnectButton.isDisplayed());
	Log.info("Fitbase App Fitbit Device Connect Button Verfied");
	
	Assert.assertTrue(device.FitbaseAppMisfitDeviceIcon.isDisplayed());
	Log.info("Fitbase App Misfit Device Icon Verfied");
		
	Assert.assertTrue(device.FitbaseAppMisfitDeviceText.isDisplayed());
	Log.info("Fitbase App Misfit Device Text Verfied");
		
	Assert.assertTrue(device.FitbaseAppMisfitDeviceConnectButton.isDisplayed());
	Log.info("Fitbase App Misfit Device Connect Button Verfied");
	
	Assert.assertTrue(device.FitbaseAppStravaDeviceIcon.isDisplayed());
	Log.info("Fitbase App Strava Device Icon Verfied");
		
	Assert.assertTrue(device.FitbaseAppStravaDeviceText.isDisplayed());
	Log.info("Fitbase App Strava Device Text Verfied");
		
	Assert.assertTrue(device.FitbaseAppStravaDeviceConnectButton.isDisplayed());
	Log.info("Fitbase App Strava Device Connect Button Verfied");
	
	Assert.assertTrue(device.FitbaseAppGoogleFitDeviceIcon.isDisplayed());
	Log.info("Fitbase App GoogleFit Device Icon Verfied");
		
	Assert.assertTrue(device.FitbaseAppGoogleFitDeviceText.isDisplayed());
	Log.info("Fitbase App GoogleFit Device Text Verfied");
		
	Assert.assertTrue(device.FitbaseAppGoogleFitDeviceConnectButton.isDisplayed());
	Log.info("Fitbase App GoogleFit Device Connect Button Verfied");
	
	Assert.assertTrue(device.FitbaseAppWithingsDeviceIcon.isDisplayed());
	Log.info("Fitbase App Withings Device Icon Verfied");
		
	Assert.assertTrue(device.FitbaseAppWithingsDeviceText.isDisplayed());
	Log.info("Fitbase App Withings Device Text Verfied");
		
	Assert.assertTrue(device.FitbaseAppWithingsDeviceConnectButton.isDisplayed());
	Log.info("Fitbase App Withings Device Connect Button Verfied");

	Thread.sleep(3000);	}

public static void fitbaseAppFitbitDeviceTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
	
	DevicesPage device = new DevicesPage(driver);
	LoginPage logout = new LoginPage(driver);
	WebDriverWait wait = new WebDriverWait(driver ,50);
			
	wait.until(ExpectedConditions.elementToBeClickable(device.FitbaseAppMenuIcon)).click();
	Log.info("Click action performed on Menu icon");
	Thread.sleep(3000);
				
	Actions.menuPageScrollup(driver);
	Thread.sleep(1500);
		
	Assert.assertTrue(device.FitbaseAppDevicesModuleButton.isDisplayed());
	device.FitbaseAppDevicesModuleButton.click();
	Log.info("Click Action Performed on Device Module Tab");

	if(device.FitbaseAppFitbitDeviceNoDeviceConnectedText.isDisplayed()==false) {
	
	Assert.assertTrue(device.FitbaseAppFitbitDeviceConnectButton.isDisplayed());
	device.FitbaseAppFitbitDeviceConnectButton.click();
	Log.info("Click Action Performed on Fitbit Device Connect Button");

	Thread.sleep(6000);
	Assert.assertTrue(device.FitbaseAppFitbitDeviceConnectionPage.isDisplayed());
	Log.info("Fitbase App Fitbit Device Connection Page Verified");
		
	Assert.assertTrue(device.FitbaseAppFitbitDeviceEmail.isDisplayed());
	device.FitbaseAppFitbitDeviceEmail.sendKeys("priyaindugula1@gmail.com");
	Log.info("Entered Valid Fitbit Account Email Address");
	
	Assert.assertTrue(device.FitbaseAppFitbitDeviceEmail.isDisplayed());
	device.FitbaseAppFitbitDevicePassword.sendKeys("password");
	Log.info("Entered Valid Fitbit Account Password");
	
	Assert.assertTrue(device.FitbaseAppFitbitDeviceLogin.isDisplayed());
	device.FitbaseAppFitbitDeviceLogin.click();
	Log.info("Click Action Performed Fitbit Device Login Button");
	
	Thread.sleep(8000);
	
}
	else {
		Log.info("Fitbit device has already connected...");
	}
	
	Thread.sleep(5000);
	device.FitbaseAppFitbitDeviceSyncDataButtonAfterConnected.click();
	Log.info("Click action performed on Fitbit Device Sync Data Button After Connected");
	
	Thread.sleep(8000);
	
	device.FitbaseAppWorkoutMenuIcon.click();
	Log.info("Click action performed on Menu icon from Device Page");
			
	Thread.sleep(1500);
	
	//Actions.menuPageScrollup(driver);
	
	//Device Workout
	device.FitbaseAppWorkoutModuleButton.click();
	Log.info("Click action performed on Menu icon from Workout Page");

	Thread.sleep(3000);
	
	Assert.assertTrue(device.FitbaseAppWorkoutCheckFitbitDeviceSyncedWorkouts.isDisplayed());
	Log.info("Fitbit Device Synched Workouts Shown in Workout Module");
	
	//Device Steps
	
	device.FitbaseAppWorkoutMenuIcon.click();
	Log.info("Click action performed on Menu icon from Workout Page");
			
	Thread.sleep(1500);
 	Actions.menuPageScrollDown(driver);
	
	device.FitbaseAppStepsModuleButton.click();
	Log.info("Click action performed on Steps Module Button");
	
	Thread.sleep(3000);
	Actions.menuPageScrollup(driver);
	
	Assert.assertTrue(device.FitbaseAppWorkoutCheckDevicesSyncedSteps.isDisplayed());
	Log.info("Fitbit Device Synched Steps Shown in  Module");
	
	device.FitbaseAppStepsMenuIcon.click();
	Log.info("Click action performed on Menu icon from Steps Page");
	
	Thread.sleep(2000);
	
//	device.FitbaseAppStepsMenuIcon.click();
//	Log.info("Click action performed on Menu icon from Steps Page");
//	
//	Thread.sleep(1000);
	
	//Dashboard
	device.FitbaseAppDashboardModuleButton.click();
	Log.info("Click action performed on Dashboard Module Button");
	
	Thread.sleep(2000);
	
	Actions.menuPageScrollup(driver);
	
	Assert.assertTrue(device.FitbaseAppDashboardFitbitDeviceSyncedSteps.isDisplayed());
	Log.info("Fitbase App Dashboard Check Fitbit Devices Synced Steps in Dashboard");
	
	device.FitbaseAppStepsMenuIcon.click();
	Log.info("Click action performed on Menu icon from Dashboard Page");
	
	Thread.sleep(2000);
	
	Actions.menuPageScrollup(driver);      
	
	Assert.assertTrue(logout.FitbaseLogoutBtn.isDisplayed());
	logout.FitbaseLogoutBtn.click();
	
	Thread.sleep(3000); }

				//FITBIT DEVICE WORKOUTS SYNCED FROM FITBIT DEVICE

public static void fitbaseAppFitbitDeviceWorkoutsTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
	DevicesPage device = new DevicesPage(driver);
	LoginPage logout = new LoginPage(driver);
		
	device.FitbaseAppMenuIcon.click();
	Log.info("Click Action performed on Menu icon");
			
	Thread.sleep(1500);
			
	Actions.menuPageScrollup(driver);
		
	Assert.assertTrue(device.FitbaseAppDevicesModuleButton.isDisplayed());
	device.FitbaseAppDevicesModuleButton.click();
	Log.info("Click Action Performed on Device Module Tab");
	Thread.sleep(2500);
	
    try {
    	if(device.FitbaseAppFitbitDeviceNoDeviceConnectedText.isDisplayed()==true) {
	
	Assert.assertTrue(device.FitbaseAppFitbitDeviceConnectButton.isDisplayed());
	device.FitbaseAppFitbitDeviceConnectButton.click();
	//Log.info("Click Action Performed on Fitbit Device Connect Button");

	Thread.sleep(6000);
	Assert.assertTrue(device.FitbaseAppFitbitDeviceConnectionPage.isDisplayed());
	Log.info("Fitbase App Fitbit Device Connection Page Verified");
		
	Assert.assertTrue(device.FitbaseAppFitbitDeviceEmail.isDisplayed());
	device.FitbaseAppFitbitDeviceEmail.sendKeys("priyaindugula1@gmail.com");
	Log.info("Entered Valid Fitbit Account Email Address");
	
	Assert.assertTrue(device.FitbaseAppFitbitDeviceEmail.isDisplayed());
	device.FitbaseAppFitbitDevicePassword.sendKeys("password");
	Log.info("Entered Valid Fitbit Account Password");
	
	Assert.assertTrue(device.FitbaseAppFitbitDeviceLogin.isDisplayed());
	device.FitbaseAppFitbitDeviceLogin.click();
	Log.info("Click Action Performed Fitbit Device Login Button");
	Thread.sleep(8000);}
    	
    	else {
    		System.out.println("Fitbit device has already connected...");
    	}
	
    }catch(Exception e) {
		System.out.println("Error Message..."+e.getMessage());
		}
    
	Thread.sleep(5000);
	device.FitbaseAppFitbitDeviceSyncDataButtonAfterConnected.click();
	Log.info("Click action performed on Fitbit Device Sync Data Button After Connected");
	
	Thread.sleep(8000);
	device.FitbaseAppWorkoutMenuIcon.click();
	Log.info("Click action performed on Menu icon from Devices Page");
			
	Thread.sleep(1500);

	device.FitbaseAppWorkoutModuleButton.click();
	Log.info("Click action performed on Workout Module Button");
	Thread.sleep(7000);
	
	try	{
		if(device.FitbaseAppWorkoutFitbitDeviceWorkoutsNotfoundText.isDisplayed()) {
		}else {
			System.out.println("Fitbit Device Synced Workouts NotFound...");
		}
	}catch(Exception e) {
		System.out.println("Error Message..."+e.getMessage());
	}
	
	Assert.assertTrue(device.FitbaseAppWorkoutCheckFitbitDeviceSyncedWorkouts.isDisplayed());
	Log.info("Fitbase App Fitbit Device Synced Workouts verified");
	Thread.sleep(4000);
	
	device.FitbaseAppWorkoutMenuIcon.click();
	Log.info("Click action performed on Menu icon from Workout Page");	
	Thread.sleep(3000);
	
	Actions.menuPageScrollup(driver);  
	
	Assert.assertTrue(logout.FitbaseLogoutBtn.isDisplayed());
	logout.FitbaseLogoutBtn.click();
	Log.info("Click action performed on Fitbase Logout ");	
	
	Thread.sleep(3000);}

					//FITBIT DEVICE STEPS SYNCED FROM FITBIT DEVICE

public static void fitbaseAppFitbitDeviceStepsTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
	DevicesPage device = new DevicesPage(driver);
	LoginPage logout = new LoginPage(driver);
		
	device.FitbaseAppMenuIcon.click();
	Log.info("Click Action performed on Menu icon");
			
	Thread.sleep(1500);
			
	Actions.menuPageScrollup(driver);
		
	Assert.assertTrue(device.FitbaseAppDevicesModuleButton.isDisplayed());
	device.FitbaseAppDevicesModuleButton.click();
	Log.info("Devices Action class Click Action Performed on Device Module Tab");
	Thread.sleep(2500);
	
	try {
	Log.info("------------------- "+driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[1]/android.widget.Button[2]\r\n" + 
			"")).isDisplayed());
	if(device.FitbaseAppFitbitDeviceDisconnectedButtonAfterConnected.isDisplayed()) {
		System.out.println("already connected");
		device.FitbaseAppFitbitDeviceDisconnectedButtonAfterConnected.click();
		Thread.sleep(2500);
	}
	}catch(Exception e) {
		System.out.println(e.getLocalizedMessage());
	}
	
	
	
	
	
	
	
	
	
	
    try {
    	Log.info("------------------- "+driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[1]/android.widget.Button[2]\r\n" + 
    			"")).isDisplayed());
    	if(device.FitbaseAppFitbitDeviceDisconnectedButtonAfterConnected.isDisplayed()) {
    		System.out.println("already connected");
    		device.FitbaseAppFitbitDeviceDisconnectedButtonAfterConnected.click();
    		Thread.sleep(2500);
    	}
    	if(device.FitbaseAppFitbitDeviceNoDeviceConnectedText.isDisplayed()==true) {
	
	Assert.assertTrue(device.FitbaseAppFitbitDeviceConnectButton.isDisplayed());
	device.FitbaseAppFitbitDeviceConnectButton.click();
	//Log.info("Click Action Performed on Fitbit Device Connect Button");

	Thread.sleep(6000);
	Assert.assertTrue(device.FitbaseAppFitbitDeviceConnectionPage.isDisplayed());
	Log.info("Fitbase App Fitbit Device Connection Page Verified");
		
	Assert.assertTrue(device.FitbaseAppFitbitDeviceEmail.isDisplayed());
	device.FitbaseAppFitbitDeviceEmail.sendKeys("priyaindugula1@gmail.com");
	Log.info("Entered Valid Fitbit Account Email Address");
	
	Assert.assertTrue(device.FitbaseAppFitbitDeviceEmail.isDisplayed());
	device.FitbaseAppFitbitDevicePassword.sendKeys("password");
	Log.info("Entered Valid Fitbit Account Password");
	
	Assert.assertTrue(device.FitbaseAppFitbitDeviceLogin.isDisplayed());
	device.FitbaseAppFitbitDeviceLogin.click();
	Log.info("Click Action Performed Fitbit Device Login Button");
	Thread.sleep(8000);}
    	
    	else {
    		System.out.println("Fitbit device has already connected...");
    	}
	
    }catch(Exception e) {
		System.out.println("Error Message..."+e.getMessage());
		}
    
	Thread.sleep(5000);
	device.FitbaseAppFitbitDeviceSyncDataButtonAfterConnected.click();
	Log.info("Click action performed on Fitbit Device Sync Data Button After Connected");
	
	Thread.sleep(8000);
	device.FitbaseAppWorkoutMenuIcon.click();
	Log.info("Click action performed on Menu icon from Devices Page");
			
	Thread.sleep(1500);

	Actions.menuPageScrollDown(driver);
	
	device.FitbaseAppStepsModuleButton.click();
	Log.info("Click action performed on Steps Module Button");
	Thread.sleep(7000);
	
	Actions.menuPageScrollup(driver);
	
	try	{
		if(device.FitbaseAppFitbitStepsModuleNoStepsFound.isDisplayed()) {
		}else {
			System.out.println("Fitbit Device Synced Steps NotFound...");
		}
	}catch(Exception e) {
		System.out.println("Error Message..."+e.getMessage());
	}
	
	Assert.assertTrue(device.FitbaseAppFitbitStepsModuleSyncedStepsFound.isDisplayed());
	Log.info("Fitbase App Fitbit Device Synced Steps verified");
	Thread.sleep(4000);
	
	device.FitbaseAppWorkoutMenuIcon.click();
	Log.info("Click action performed on Menu icon from Workout Page");	
	Thread.sleep(3000);
	
	Actions.menuPageScrollup(driver);  
	
	Assert.assertTrue(logout.FitbaseLogoutBtn.isDisplayed());
	logout.FitbaseLogoutBtn.click();
	Log.info("Click action performed on Fitbase Logout ");	
	
	Thread.sleep(3000);}

				//FITBIT DEVICE DASHBOARD STEPS SYNCED FROM FITBIT DEVICE

public static void fitbaseAppFitbitDevicedDashboardStepsTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
	DevicesPage device = new DevicesPage(driver);
	LoginPage logout = new LoginPage(driver);

	device.FitbaseAppMenuIcon.click();
	Log.info("Click Action performed on Menu icon");

	Thread.sleep(1500);

	Actions.menuPageScrollup(driver);

	Assert.assertTrue(device.FitbaseAppDevicesModuleButton.isDisplayed());
	device.FitbaseAppDevicesModuleButton.click();
	Log.info("Click Action Performed on Device Module Tab");
	Thread.sleep(2500);
try {
			if(device.FitbaseAppFitbitDeviceDisconnectedButtonAfterConnected.isDisplayed()) {
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[1]/android.widget.Button[2]\r\n" + "")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.app.Dialog/android.app.Dialog/android.view.View[1]/android.widget.Button")).click();
				Thread.sleep(2500);
			}
			}catch(Exception e) {
				System.out.println(e.getLocalizedMessage());
			}
	try {
		
		if(device.FitbaseAppFitbitDeviceNoDeviceConnectedText.isDisplayed()) {

			Assert.assertTrue(device.FitbaseAppFitbitDeviceConnectButton.isDisplayed());
			device.FitbaseAppFitbitDeviceConnectButton.click();
			//Log.info("Click Action Performed on Fitbit Device Connect Button");

			Thread.sleep(10000);
			Assert.assertTrue(device.FitbaseAppFitbitDeviceConnectionPage.isDisplayed());
			Log.info("Fitbase App Fitbit Device Connection Page Verified");

			Assert.assertTrue(device.FitbaseAppFitbitDeviceEmail.isDisplayed());
			device.FitbaseAppFitbitDeviceEmail.sendKeys("priyaindugula1@gmail.com");
			Log.info("Entered Valid Fitbit Account Email Address");

			Assert.assertTrue(device.FitbaseAppFitbitDeviceEmail.isDisplayed());
			device.FitbaseAppFitbitDevicePassword.sendKeys("password");
			Log.info("Entered Valid Fitbit Account Password");

			Assert.assertTrue(device.FitbaseAppFitbitDeviceLogin.isDisplayed());
			device.FitbaseAppFitbitDeviceLogin.click();
			Log.info("Click Action Performed Fitbit Device Login Button");
			Thread.sleep(8000);}

		else {
			System.out.println("Fitbit device has already connected...");
		}

	}catch(Exception e) {
		System.out.println("Error Message..."+e.getMessage());
	}

	Thread.sleep(5000);
	device.FitbaseAppFitbitDeviceSyncDataButtonAfterConnected.click();
	Log.info("Click action performed on Fitbit Device Sync Data Button After Connected");

	Thread.sleep(8000);
	device.FitbaseAppWorkoutMenuIcon.click();
	Log.info("Click action performed on Menu icon from Devices Page");

	Thread.sleep(1500);

	Actions.menuPageScrollDown(driver);

	device.FitbaseAppDashboardModuleButton.click();
	Log.info("Click action performed on Dashboard Module Button");
	Thread.sleep(7000);

	Actions.menuPageScrollup(driver);

	try	{
		if(device.FitbaseAppDashboardFitbitNoSyncedDeviceSteps.isDisplayed()) {
		}else {
			System.out.println("Fitbit Device Synced Steps NotFound In Dashboard...");
		}
	}catch(Exception e) {
		System.out.println("Error Message..."+e.getMessage());
	}

	Assert.assertTrue(device.FitbaseAppDashboardFitbitDeviceSyncedSteps.isDisplayed());
	Log.info("Fitbase App Fitbit Device Dashboard Synced Steps verified");
	Thread.sleep(4000);

	device.FitbaseAppDashboardMenuIcon.click();
	Log.info("Click action performed on Menu icon from Dashboard Page");
	Thread.sleep(3000);

	Actions.menuPageScrollup(driver);  

	Assert.assertTrue(logout.FitbaseLogoutBtn.isDisplayed());
	logout.FitbaseLogoutBtn.click();
	Log.info("Click action performed on Fitbase Logout ");	

	Thread.sleep(3000);}
}
