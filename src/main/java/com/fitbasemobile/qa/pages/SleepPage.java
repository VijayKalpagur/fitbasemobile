package com.fitbasemobile.qa.pages;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SleepPage {
	
public SleepPage(AndroidDriver<AndroidElement> driver){
		
	PageFactory.initElements(new AppiumFieldDecorator(driver),this);
}

//----------------------------# SLEEP DASHBOARD #-------------------------------	
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[1]/android.view.View/android.widget.Button\r\n" + "")
	public AndroidElement FitbaseAppMenuIcon;	
	
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[7]/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppSleepBtn;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[2]/android.view.View\r\n" + "")
	public AndroidElement FitbaseAppSleepHeadeText;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[3]/android.view.View/android.view.View/android.widget.Button\r\n" + "")
	public AndroidElement FitbaseAppLogSleepLink;
	
//	@AndroidFindBy(xpath ="")
//	public AndroidElement FitbaseAppSleepReminderIcon;
	
// ----------------------------# DAY SLEEP DASHBOARD #-------------------------------	
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[1]/android.widget.Button\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTabText;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[1]/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodayText;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodayForwardBackwardicon;
		
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[2]/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTimeRecommendedText;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[3]/android.widget.Image\r\n" + "")
	public AndroidElement FitbaseAppDaySleepCoudMoonIcon;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[4]/android.view.View/android.widget.Image/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTotalSleptTimeText;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[5]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepAwakeTextandTime;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[5]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepLightTextandTime;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[5]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepDeepTextandTime;
	
//	@AndroidFindBy(xpath ="")
//	public AndroidElement FitbaseAppSleepReminderIcon;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[8]/android.widget.Image\r\n" + "")
	public AndroidElement FitbaseAppDaySleepGraph;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[8]/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepMoonIconWithtime;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[8]/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepSunIconWithtime;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[9]/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepWakeLightDeepText;	
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[8]/android.widget.Image\r\n" + "")
	public AndroidElement FitbaseAppDaySleepPercentageSleepEfficiency;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[8]/android.widget.Image\r\n" + "")
	public AndroidElement FitbaseAppDaySleepMoodIcon;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[8]/android.widget.Image\r\n" + "")
	public AndroidElement FitbaseAppDaySleepMoodText;
	
	
// ----------------------------# ADD SLEEP PAGE #-------------------------------
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[2]/android.app.Dialog/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[2]/android.view.View\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepPopupWindow;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[2]/android.app.Dialog/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[2]/android.view.View\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepPageHeaderText;	
//----------------------	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[3]\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepPageCancelIcon;
	
//	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[3]/android.view.View/android.view.View/android.widget.Button\r\n" + "")
//	public AndroidElement FitbaseAppAddDrinkPageCancelIcon;
//	
//	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[3]/android.view.View/android.view.View/android.widget.Button\r\n" + "")
//	public AndroidElement FitbaseAppAddDrinkPageCancelIcon;
	
//---------------------	
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[2]/android.app.Dialog/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepPageDate;
		
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[2]/android.app.Dialog/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[2]/android.view.View[1]/android.view.View/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepPageBedTime;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[2]/android.app.Dialog/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[2]/android.view.View[1]/android.view.View/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepPageBedTimeIcon;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[2]/android.app.Dialog/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[2]/android.view.View[1]/android.view.View/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepPageWakeUpTime;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[2]/android.app.Dialog/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[2]/android.view.View[1]/android.view.View/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepPageWakeUpTimeIcon;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[2]/android.app.Dialog/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[3]/android.view.View[1]/android.view.View/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepPageSleepDuration;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[2]/android.app.Dialog/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[3]/android.view.View[1]/android.view.View/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepPageSleepDurationIcon;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[2]/android.app.Dialog/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[4]/android.widget.Button\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepPageSaveButton;
	
// ----------------------------# ADD SLEEP/LOG PAGE - SELECT DATE/TIME/LOG SLEEP #-------------------------------	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[3]/android.view.View/android.view.View/android.widget.Button\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepLink;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[2]/android.app.Dialog/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[2]\r\n" + "")
	public AndroidElement  FitbaseAppDayLogSleepPageSelectDate;
	
	@AndroidFindBy(xpath ="//android.view.View[@content-desc=\"12 December 2019\"]\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepPageSelectDateInPopupCalender;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[1]\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepPageSelectDateInPopupCalenderCancleButton;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepPageSelectDateInPopupCalenderOkButton;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[2]/android.app.Dialog/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[2]/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepPageLogBedTime;
	
	@AndroidFindBy(xpath ="//android.widget.RadialTimePickerView.RadialPickerTouchHelper[@content-desc=\"5\"]\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepPageSelectBedTimeinPopupClock;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepPageSelectBedTimeinPopupClockOkButton;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[2]/android.app.Dialog/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[3]/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepPageLogWakeUpTime;
	
	@AndroidFindBy(xpath ="//android.widget.RadialTimePickerView.RadialPickerTouchHelper[@content-desc=\"5\"]\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepPageSelectWakeUpTimeinPopupClock;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]\r\n" + "")
	public AndroidElement FitbaseAppDayLogSleepPageSelectWakeUpTimeinPopupClockOkButton;
	
// ----------------------------# DAY SLEEP DASHBOARD - SET REMINDER PAGE #-------------------------------		
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayou")
	public AndroidElement FitbaseAppDaySleepSetRemindericon;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderHeaderText;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderBackArrowIcon;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderSleepReminderText;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderDoggleEnableReminderButton;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderDoggleAlramSoundButton;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderAddNewReminderAddIcon;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderAddNewReminderTime;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderAddNewReminderTimeDogleButton;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderAddNewReminderDaySelction;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderAddNewReminderTimeicon;
	
// ----------------------------# DAY SLEEP DASHBOARD - SET REMINDER POP UP PAGE #-------------------------------		
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSleepReminderPopupPage;
		
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSelectSleepReminderType;
		
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSelectSleepReminderSleepBox;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSelectSleepReminderSleepRadioButton;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSelectSleepReminderWakeUpBox;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSelectSleepReminderWakeUpRadioButton;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSleepReminderTime;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSelectSleepSelectReminderTime;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSleepRemindMeBefore;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSelectSleepRemindMeBeforeTime;

	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSleepRepeat;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSleepRepeatAll;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderAddNewReminderPopupSleepSelectDay;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderAddNewReminderSleepPopupCancelButton;
		
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/andro")
	public AndroidElement FitbaseAppDaySleepPageSetReminderAddNewReminderSleepPopupOkButton;
		
// ----------------------------# DAY SLEEP DASHBOARD - TODAY'S LOGGED SLEEP PAGE #-------------------------------		
//	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[9]/android.view.View[2]\r\n" + "")
//	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepHeaderText;
//	
//	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[10]/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View/android.widget.Image\r\n" + "")
//	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepCloudMoonIcon;
//	
//	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[10]/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[2]/android.view.View[1]\r\n" + "")
//	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepTimeRange;
//	
//	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[10]/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[2]/android.view.View[1]\r\n" + "")
//	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepTime;
//
//	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android")
//	public AndroidElement  FitbaseAppDaySleepTodaysRecentlyLoggedSleepThruDevices;
//	
// ----------------------------# DAY SLEEP DASHBOARD ->  EDIT - TODAY'S LOGGED SLEEP #-------------------------------		
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[10]/android.view.View[1]/android.view.View[1]/android.view.View/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepSwipe;
		
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[10]/android.view.View[2]/android.widget.Button[1]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepEdit;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[10]/android.view.View[2]/android.widget.Button[2]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepDelete;
		
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[2]/android.app.Dialog/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[2]/android.view.View\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPage; 
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[4]/android.view.View/android.view.View[1]/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageBackArrowIcon; 
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[2]/android.app.Dialog/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[2]\r\n" + "")
	public AndroidElement  FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectDate;
	
	@AndroidFindBy(xpath ="//android.view.View[@content-desc=\"12 December 2019\"]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectDateInPopupCalender;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[1]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectDateInPopupCalenderCancleButton;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectDateInPopupCalenderOkButton;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[2]/android.app.Dialog/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[2]/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageLogBedTime;
	
	@AndroidFindBy(xpath ="//android.widget.RadialTimePickerView.RadialPickerTouchHelper[@content-desc=\"5\"]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectBedTimeinPopupClock;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectBedTimeinPopupClockOkButton;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[2]/android.app.Dialog/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[3]/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageLogWakeUpTime;
	
	@AndroidFindBy(xpath ="//android.widget.RadialTimePickerView.RadialPickerTouchHelper[@content-desc=\"5\"]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectWakeUpTimeinPopupClock;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepEditLogSleepPageSelectWakeUpTimeinPopupClockOkButton;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[10]/android.view.View[1]/android.view.View[1]/android.view.View/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepDeviceIcon;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[10]/android.view.View[1]/android.view.View[1]/android.view.View/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepUpdateButton;
	
// ----------------------------# WEEK SLEEP DASHBOARD #-------------------------------		
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[1]/android.widget.Button\r\n" + "")
	public AndroidElement FitbaseAppWeekTabText;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[1]/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View\r\n" + "")
	public AndroidElement FitbaseAppWeekDate;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppWeekForwardBackwardicon;
		
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[2]/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppWeekSleepPercentageEfficencyText;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[3]/android.widget.Image\r\n" + "")
	public AndroidElement FitbaseAppWeekSleepCoudMoonIcon;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[4]/android.view.View/android.widget.Image/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppWeekSleepDailyAverageTimeText;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[4]/android.view.View/android.widget.Image/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppWeekSleepWeeklyAverageAwakeTimeText;
		
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[5]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepWeeklyAverageLightTextandTime;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[5]\r\n" + "")
	public AndroidElement FitbaseAppWeekSleepAverageDeepTextandTime;
		
//	@AndroidFindBy(xpath ="")
//	public AndroidElement FitbaseAppSleepReminderIcon;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[8]/android.widget.Image\r\n" + "")
	public AndroidElement FitbaseAppWeekSleepGraph;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[8]/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppWeekSleepMoonIconWithtime;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[8]/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppWeekSleepSunIconWithtime;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[9]/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppWeekSleepWakeLightDeepText;	
	
//	@AndroidFindBy(xpath ="")
//	public AndroidElement FitbaseAppWeekLoggedNumberofGlassesThruDevices;
	
//// ----------------------------# MONTHLY SLEEP DASHBOARD #-------------------------------		
//		
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[1]/android.widget.Button\r\n" + "")
	public AndroidElement FitbaseAppMonthTabText;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[1]/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View\r\n" + "")
	public AndroidElement FitbaseAppMonthText;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppMonthForwardBackwardicon;
		
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[2]/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppMonthSleepTimeRecommendedText;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[3]/android.widget.Image\r\n" + "")
	public AndroidElement FitbaseAppMonthSleepCoudMoonIcon;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[4]/android.view.View/android.widget.Image/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppMonthSleepToatalSleptTimeText;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[5]\r\n" + "")
	public AndroidElement FitbaseAppMonthSleepAwakeTextandTime;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[5]\r\n" + "")
	public AndroidElement FitbaseAppMonthSleepLightTextandTime;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[5]\r\n" + "")
	public AndroidElement FitbaseAppMonthSleepDeepTextandTime;
	
//	@AndroidFindBy(xpath ="")
//	public AndroidElement FitbaseAppSleepReminderIcon;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[8]/android.widget.Image\r\n" + "")
	public AndroidElement FitbaseAppMonthSleepGraph;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[8]/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppMonthSleepMoonIconWithtime;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[8]/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppMonthSleepSunIconWithtime;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[9]/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppMonthSleepWakeLightDeepText;	
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[8]/android.widget.Image\r\n" + "")
	public AndroidElement FitbaseAppMonthSleepSPercentageSleepEfficiency;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[9]/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepHeaderText;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[10]/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View/android.widget.Image\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepCloudMoonIcon;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[10]/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[2]/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepTimeRange;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[10]/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[2]/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppDaySleepTodaysRecentlyLoggedSleepTime;

	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android")
	public AndroidElement  FitbaseAppDaySleepTodaysRecentlyLoggedSleepThruDevices;
	
//	@AndroidFindBy(xpath ="")
//	public AndroidElement FitbaseAppMonthLoggedNumberofGlassesThruDevices;
//		
//// ----------------------------# YEARLY SLEEP DASHBOARD #-------------------------------		
//		
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[1]/android.widget.Button\r\n" + "")
	public AndroidElement FitbaseAppYearTabText;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[1]/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View\r\n" + "")
	public AndroidElement FitbaseAppYearText;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppYearForwardBackwardicon;
		
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[2]/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppYearSleepTimeRecommendedText;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[3]/android.widget.Image\r\n" + "")
	public AndroidElement FitbaseAppYearSleepCoudMoonIcon;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[4]/android.view.View/android.widget.Image/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppYearSleepToatalSleptTimeText;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[5]\r\n" + "")
	public AndroidElement FitbaseAppYearSleepAwakeTextandTime;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[5]\r\n" + "")
	public AndroidElement FitbaseAppYearSleepLightTextandTime;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[5]\r\n" + "")
	public AndroidElement FitbaseAppYearSleepDeepTextandTime;
	
//	@AndroidFindBy(xpath ="")
//	public AndroidElement FitbaseAppSleepReminderIcon;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[8]/android.widget.Image\r\n" + "")
	public AndroidElement FitbaseAppYearSleepGraph;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[8]/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppYearSleepMoonIconWithtime;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[8]/android.view.View[2]\r\n" + "")
	public AndroidElement FitbaseAppYearSleepSunIconWithtime;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[9]/android.view.View[1]\r\n" + "")
	public AndroidElement FitbaseAppYearSleepWakeLightDeepText;	
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[8]/android.widget.Image\r\n" + "")
	public AndroidElement FitbaseAppMonthSleepSPercentageleepEfficiency;
	
//	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[8]/android.widget.Image\r\n" + "")
//	public AndroidElement FitbaseAppYearSleepMoodIcon;
	
	@AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[8]/android.widget.Image\r\n" + "")
	public AndroidElement FitbaseAppYearSleepMoodText;
	
//	@AndroidFindBy(xpath ="")
//	public AndroidElement FitbaseAppYearLoggedNumberofGlassesThruDevices;
//		
//				
////------------------------# LOGOUT #-----------------------------------	
//
//	@FindBy(how=How.XPATH, using ="//li[@class='nav-item dropdown user-d']//a")
//	public WebElement LogoutClickonUserProfile;
//			
//	@FindBy(how=How.XPATH, using = "//ul[@class='dropdown-menu animate slideIn show']/li[2]/a" )
//	public WebElement ClickonLogout;
//	
}
